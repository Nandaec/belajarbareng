<?php
$obj_pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$obj_pdf->SetCreator(PDF_CREATOR);
$title = "RSI KLATEN";
$obj_pdf->SetTitle($title);
$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title);
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, 0, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->SetPrintHeader(false);
$obj_pdf->AddPage();
ob_start();
?>

<style type="text/css">
	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}
	.form-surat {  }
	h3 { text-align: center; }
	.garis { width: 100%; height: 10px; border-top: 2px solid black; }
	.font { font-weight: normal; font-size: 12px; line-height: 20px; }
</style>

<?php foreach ($pesan as $pes) { 
	$id_karyawan = $pes->id_record_karyawan;
	$judul = $pes->judul;
}; ?>

<div class="body">
	<div class="form-surat">
		<h3>BIDANG / DIVISI / INSTALASI / KOMITE / PANITIA / TIM <br>RSU ISLAM KLATEN</h3>
		<div class="garis"></div>
		<div class="font">
			Kepada : <br>
			Direktur Utama <br>
			Ditempat
			<br><br>
			Assalamualaikum wr.wb
			<br><br>
			Bersama ini kami sampaikan laporan kinerja organisasi ... <br>
			Bulan 
			...
			tahun 
			...
			seperti laporan terlampir.<br>

			Kesimpulan :
			<br>
			Tindakan :
			<br>
			Demikian untuk menjadikan periksa, atas perhatiannya disampaikan terima kasih.
			<br><br>
			Wassalamualaikum wr.wb
			<br><br>
			<table class="tanda">
				<tr>
					<th>Mengetahui</th>
					<th>Klaten, </th>
				</tr>
				<tr>
					<th>...</th>
					<th>...</th>
				</tr>
				<tr>
					<th></th>
					<th></th>
				</tr>
				<tr class="tanda-ttd">
					<th>...</th>
					<th>...</th>
				</tr>
				<tr>
					<th>dr. H Sutrisno, M.Kes.</th>
					<th>...</th>
				</tr>
			</table>
		</div>
	</div>
</div>

<?php
$content = ob_get_contents();
ob_end_clean();
$obj_pdf->writeHTML($content, true, false, true, false, '');
$obj_pdf->Output('output.pdf', 'I');?>