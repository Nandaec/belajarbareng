<!DOCTYPE html>
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- start: HEAD -->
<head>
    <title>Disposisi Online</title>
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/backoffice/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/backoffice/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/backoffice/fonts/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/backoffice/css/main.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/backoffice/css/main-responsive.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/backoffice/plugins/iCheck/skins/all.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/backoffice/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/backoffice/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/backoffice/css/theme_light.css" type="text/css" id="skin_color">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/backoffice/plugins/select2/select2.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/backoffice/plugins/datepicker/css/datepicker.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/backoffice/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/backoffice/css/print.css" type="text/css" media="print"/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/backoffice/plugins/toastr/toastr.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/backoffice/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
    <link href="<?php echo base_url();?>assets/backoffice/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>assets/backoffice/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/backoffice/plugins/font-awesome/css/font-awesome-ie7.min.css">
    <![endif]-->
    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/backoffice/plugins/fullcalendar/fullcalendar/fullcalendar.css">
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link rel="icon" href="<?php echo base_url();?>assets/backoffice/images/user.ico">
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>
    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        <div class="container">
            <div class="navbar-header">
                <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="clip-list-2"></span>
                </button>
                <a class="navbar-brand" href="index.html">
                    <img src="<?php echo base_url();?>assets/backoffice/images/logobaru.png" alt="" style="width: 230px; margin-top: -10px;">
                </a>
            </div>
            <div class="navbar-tools">
                <ul class="nav navbar-right">
                    <li class="dropdown current-user">
                        <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
                            <img src="<?php echo base_url();?>assets/backoffice/images/avatar-1-small.jpg" class="circle-img" alt="">
                            <span class="username"><?php echo $nama = ucwords($this->session->userdata('nama'));?></span>
                            <i class="clip-chevron-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?php echo base_url();?>login/logout">
                                    <i class="clip-exit"></i>
                                    &nbsp;Log Out
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="main-container">
        <div class="navbar-content">
            <div class="main-navigation navbar-collapse collapse">
                <div class="navigation-toggler">
                    <i class="clip-chevron-left"></i>
                    <i class="clip-chevron-right"></i>
                </div>
                <ul class="main-navigation-menu">
                    <li class="<?php if($page == "dashboard") { echo 'active open'; }; ?>">
                        <a href="<?php echo base_url();?>dashboard"><i class="clip-home-3"></i>
                            <span class="title"> Dashboard </span><span class="selected"></span>
                        </a>
                    </li>
                    <!-- <li class="<?php if($page == "dunit") { echo 'active open'; }; ?>">
                        <a href="javascript:void(0)"><i class="clip-screen"></i>
                            <span class="title"> Data Master </span><i class="icon-arrow"></i>
                            <span class="selected"></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="<?php echo base_url();?>dunit">
                                    <span class="title"> BON Operasi </span>
                                </a>
                            </li>
                        </ul>
                    </li> -->
                    <li class="<?php if($page == "disposisi") { echo 'active open'; }; ?>">
                        <a href="<?php echo base_url();?>disposisi"><i class="clip-file"></i></i>
                            <span class="title"> Tulis Disposisi </span><span class="selected"></span>
                        </a>
                    </li>
                    <li class="<?php if($page == "pesan") { echo 'active open'; }; ?>">
                        <a href="<?php echo base_url();?>pesan"><i class="fa fa-envelope-o"></i>
                            <span class="title"> Kotak Masuk </span><span class="selected"></span>
                        </a>
                    </li>
                    <li>
                        <?php $id_unit = $this->session->userdata('id_unit');?>
                        <a href="javascript:void(0)"><i class="clip-data"></i>
                            <span class="title"> Data Master </span><i class="icon-arrow"></i>
                            <span class="selected"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="<?php if($page == "master/sarmut") { echo 'active open'; }; ?>">
                                <a href="<?php echo base_url();?>master/sarmut/<?php echo $id_unit;?>">
                                    <span class="title"> Sasaran Mutu </span>
                                </a>
                            </li>
                            <li class="<?php if($page == "master/program_unit") { echo 'active open'; }; ?>">
                                <a href="<?php echo base_url();?>master/program_unit/<?php echo $id_unit;?>">
                                    <span class="title"> Program Unit </span>
                                </a>
                            </li>
                            <li class="<?php if($page == "master/program_diluar_unit") { echo 'active open'; }; ?>">
                                <a href="<?php echo base_url();?>master/program_diluar_unit/<?php echo $id_unit;?>">
                                    <span class="title"> Program Diluar Unit </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>

        <div class="main-content">
            <div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;
                            </button>
                            <h4 class="modal-title">Panel Configuration</h4>
                        </div>
                        <div class="modal-body">
                            Here will be a configuration form
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                Close
                            </button>
                            <button type="button" class="btn btn-primary">
                                Save changes
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <!-- start: PAGE HEADER -->
                <?php if ($this->session->flashdata('success-message') == TRUE) {; ?>
                <div class="alert alert-info">
                    <button data-dismiss="alert" class="close"> &times; </button>
                    <i class="fa fa-info-circle"></i>
                    <?php echo $this->session->flashdata('success-message'); ?>
                </div>
                <?php } if ($this->session->flashdata('danger-message') == TRUE) {; ?>
                <div class="alert alert-danger">
                    <button data-dismiss="alert" class="close"> &times; </button>
                    <i class="fa fa-info-circle"></i>
                    <?php echo $this->session->flashdata('danger-message'); ?>
                </div>
                <?php }; ?>
                <?php $this->load->view($page); ?>
                <!-- end: PAGE CONTENT-->
            </div>
        </div>
    </div>

    <div class="footer clearfix">
        <div class="footer-inner">
            2014 &copy; clip-one by cliptheme.
        </div>
        <div class="footer-items">
            <span class="go-top"><i class="clip-chevron-up"></i></span>
        </div>
    </div>
    <!-- end: FOOTER -->
    <div id="event-management" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Event Management</h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                        Close
                    </button>
                    <button type="button" class="btn btn-danger remove-event no-display">
                        <i class='fa fa-trash-o'></i> Delete Event
                    </button>
                    <button type='submit' class='btn btn-success save-event'>
                        <i class='fa fa-check'></i> Save
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
    <script src="<?php echo base_url();?>assets/backoffice/plugins/respond.min.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/plugins/excanvas.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <![endif]-->
    <!--[if gte IE 9]><!-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <!--<![endif]-->
    <script src="<?php echo base_url();?>assets/backoffice/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/plugins/blockUI/jquery.blockUI.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/plugins/iCheck/jquery.icheck.min.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/plugins/less/less-1.5.0.min.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/plugins/jquery-cookie/jquery.cookie.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/js/main.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <script src="<?php echo base_url();?>assets/backoffice/plugins/jquery.sparkline/jquery.sparkline.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/plugins/fullcalendar/fullcalendar/fullcalendar.js"></script>

    <script src="<?php echo base_url();?>assets/backoffice/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/plugins/autosize/jquery.autosize.min.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/plugins/select2/select2.min.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/plugins/jquery.maskedinput/src/jquery.maskedinput.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/plugins/jquery-maskmoney/jquery.maskMoney.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/plugins/bootstrap-daterangepicker/moment.min.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/plugins/bootstrap-colorpicker/js/commits.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/plugins/summernote/build/summernote.min.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/plugins/ckeditor/ckeditor.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/plugins/ckeditor/adapters/jquery.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/js/form-elements.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/js/ui-modals.js"></script>
    <script src="<?php echo base_url();?>assets/backoffice/plugins/toastr/toastr.min.js"></script>

    <script>
        jQuery(document).ready(function() {
            Main.init();
            FormElements.init();
        });
    </script>
    <script>
        CKEDITOR.env.isCompatible = true;
    </script>

    <script>
        $(function() {

            // Toast Message
            <?php if ($this->session->flashdata('toast-success')  == TRUE) { ?>
                toastr.success('<?php echo $this->session->flashdata('toast-success'); ?>')
            <?php } if ($this->session->flashdata('toast-info') == TRUE) { ?>
                toastr.info('<?php echo $this->session->flashdata('toast-info'); ?>')
            <?php } if ($this->session->flashdata('toast-error') == TRUE) { ?>
                toastr.error('<?php echo $this->session->flashdata('toast-error'); ?>')
            <?php } if ($this->session->flashdata('toast-warning') == TRUE) { ?>
                toastr.warning('<?php echo $this->session->flashdata('toast-warning'); ?>')
            <?php }; ?>
            })
    </script>

</body>
<!-- end: BODY -->
</html>