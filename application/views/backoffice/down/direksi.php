<div class="row">
	<div class="col-sm-12">
		<div class="page-header">
			<h1><?php echo $title;?></h1>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-3 pull-right">
		<form method="POST" action="<?php echo site_url('admin/direksi/search');?>">
			<div class="input-group">
				<input type="text" name="search" onkeyup="pencarian_tabel()" class="form-control" placeholder="Search..." autocomplete="off">
				<span class="input-group-btn">
					<button class="btn btn-primary" type="submit"> <i class="fa fa-search"></i> </button>
				</span>
			</div>
		</form>
	</div>
</div>

<br>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<i class="fa fa-external-link-square"></i>
				<?php echo $description;?>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover" id="sample-table-2">
						<thead>
							<tr>
								<th class="center">NO</th>
								<th>Direksi</th>
								<th>Nama Direktur</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php if (empty($direksi)) { ?>
							<tr><td colspan="12" style="text-align: center; font-weight: bold">-- Data direksi kosong --</td></tr>
							<?php } else { ?>
							<?php $nomor=0; foreach ($direksi as $row): ?>
							<tr>
								<td class="text-center">
									<?php echo $nomor=$nomor+1; ?>
								</td>
								<td>
									<?php echo $row->nama; ?>
								</td>
								<td>
									<?php echo $row->nama_lengkap; ?>
								</td>
								<td class="center">
									<a href="#edit<?php echo $row->id_record;?>" data-toggle="modal" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
									<a href="#hapus<?php echo $row->id_record;?>" data-toggle="modal" class="btn btn-xs btn-bricky tooltips" data-placement="top" data-original-title="Hapus"><i class="fa fa-times fa fa-white"></i></a>
								</td>
							</tr>
							<?php endforeach; ?>
							<?php }; ?>
						</tbody>
					</table>
					<div class="text-center mt-3">
						<?php echo $this->pagination->create_links(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php foreach ($direksi as $row): ?>
	<div id="edit<?php echo $row->id_record;?>" class="modal fade" data-width="760" style="display: none;">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				&times;
			</button>
			<h4 class="modal-title">Edit Direksi</h4>
		</div>
		<form method="post" role="form" action="<?php echo base_url();?>admin/direksi/edit_direksi">
			<div class="modal-body">
				<div class="form-group">
					<label>Direksi</label>
					<input type="text" name="nama" value="<?php echo $row->nama;?>" class="form-control" required="" autocomplete="off">
				</div>
				<div class="form-group">
					<label>Nama Direktur</label>
					<input type="text" name="nama_lengkap" value="<?php echo $row->nama_lengkap;?>" class="form-control" required="" autocomplete="off">
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="id_record" value="<?php echo $row->id_record;?>" class="form-control">
				<button type="button" data-dismiss="modal" class="btn btn-default">Batal</button>
				<button type="submit" class="btn btn-primary">Simpan</button>
			</div>
		</form>
	</div>

	<div id="hapus<?php echo $row->id_record;?>" class="modal fade" data-backdrop="static" data-keyboard="false" style="display: none;">
		<div class="modal-body">
			<?php $data = $this->db->query("SELECT * FROM online_master_direksi where id_record = '$row->id_record'")->row_array();?>
			Hapus data direksi <strong><?php echo $data['nama'];?> </strong>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-default"> Batal </button>
			<button type="button" data-dismiss="modal" class="btn btn-danger" onclick="location.href='<?php echo base_url();?>admin/direksi/hapus/<?php echo $row->id_record;?>';"> Hapus </button>
		</div>
	</div>

<?php endforeach; ?>