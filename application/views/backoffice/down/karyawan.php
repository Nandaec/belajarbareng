<div class="row">
	<div class="col-sm-12">
		<div class="page-header">
			<h1><?php echo $title;?></h1>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-9 mb-3 mb-md-0">
		<a href="#tambah" data-toggle="modal" class="btn btn-sm btn-primary">
			<i class="glyphicon glyphicon-plus"></i> Tambah Karyawan
		</a>
	</div>
	<div class="col-md-3 pull-right">
		<form method="POST" action="<?php echo site_url('admin/karyawan/search');?>">
			<div class="input-group">
				<input type="text" name="search" onkeyup="pencarian_tabel()" class="form-control" placeholder="Pencarian" autocomplete="off">
				<span class="input-group-btn">
					<button class="btn btn-primary" type="submit"> <i class="fa fa-search"></i> </button>
				</span>
			</div>
		</form>
	</div>
</div>

<br>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<i class="fa fa-external-link-square"></i>
				<?php echo $description;?>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover" id="sample-table-2">
						<thead>
							<tr>
								<th class="center">NO</th>
								<th>Nama</th>
								<th>Unit</th>
								<th>Penempatan</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php if (empty($karyawan)) { ?>
							<tr><td colspan="12" style="text-align: center; font-weight: bold">-- Data karyawan kosong --</td></tr>
							<?php } else { ?>
							<?php $nomor=0; foreach ($karyawan as $row): ?>
							<tr>
								<td class="text-center">
									<?php echo $nomor=$nomor+1; ?>
								</td>
								<td>
									<?php echo $row->nama; ?>
								</td>
								<td>
									<?php echo $row->unit; ?>
								</td>
								<td> <?php echo $row->jabatan; ?> </td>
								<td class="center">
									<a href="#edit<?php echo $row->id_record;?>" data-toggle="modal" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
									<a href="#hapus<?php echo $row->id_record;?>" data-toggle="modal" class="btn btn-xs btn-bricky tooltips" data-placement="top" data-original-title="Hapus"><i class="fa fa-times fa fa-white"></i></a>
								</td>
							</tr>
							<?php endforeach; ?>
							<?php }; ?>
						</tbody>
					</table>
					<div class="text-center mt-3">
						<?php echo $this->pagination->create_links(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="tambah" class="modal fade" data-width="760" style="display: none;">
	<?php $data = $this->db->query("SELECT * FROM online_master_karyawan order by id_record asc")->result(); ?>
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
			&times;
		</button>
		<h4 class="modal-title">Tambah Karyawan</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-5">
				<label>NIP Karyawan</label>
				<p>
					<div class="input-group">
						<input type="text" name="nip" id="nip" class="form-control" placeholder="NIP Karyawan" autocomplete="off">
						<span class="input-group-btn">
							<button class="btn btn-primary" id="cari" type="submit"> <i class="fa fa-search">Cari</i> </button>
						</span>
						<span id="cek_nip_feedback"></span>
					</div>
				</p>
			</div>

			<form method="post" role="form" class="form-horizontal" action="<?php echo base_url();?>admin/karyawan/tambah_karyawan">
				<div class="col-sm-7">
					<table class="table table-condensed table-hover">
						<thead>
							<tr>
								<th colspan="3">Data Karyawan</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width="20%">NIP</td>
								<td>: </td>
								<td><input type="text" name="nip2" id="nip2" class="form-select" readonly=""></td>
							</tr>
							<tr>
								<td>Nama</td>
								<td>: </td>
								<td><input type="text" name="nama" id="nama" class="form-select" readonly="" style="width: 100%;"></td>
							</tr>
							<tr>
								<td>Unit</td>
								<td>: </td>
								<td><input type="text" name="unit" id="unit" class="form-select" readonly="" style="width: 100%;"></td>
							</tr>
							<tr>
								<td>Penempatan</td>
								<td>: </td>
								<td><input type="text" name="jabatan" id="jabatan" class="form-select" readonly="" style="width: 100%;"></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="pull-right" id="submit" style="display: none;">
					<button type="button" data-dismiss="modal" class="btn btn-default">Batal</button>
					<button type="submit" class="btn btn-primary">Tambah</button>
				</div>
			</form>
		</div>
	</div>	
</div>

<?php foreach ($karyawan as $row): ?>
	<div id="edit<?php echo $row->id_record;?>" class="modal fade" data-width="760" style="display: none;">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				&times;
			</button>
			<h4 class="modal-title">Edit Karyawan</h4>
		</div>
		<form method="post" role="form" class="form-horizontal" action="<?php echo base_url();?>admin/karyawan/edit_karyawan">
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<label>Unit Sekretariat</label>
						<p>
							<select name="unit_sekretariat[]" multiple="multiple" class="form-control search-select" required="">								
								<?php foreach ($unit_penempatan as $pen) { ?>
								<?php $dunit = $this->db->query("SELECT * FROM online_master_unit where id_record = '$pen->id_record_unit'")->row();?>
								<option value="<?php echo $pen->id_record; ?>"><?php echo $pen->unit; ?></option>
								?>

								<?php }; ?>
							</select>
						</p>
					</div>
					<div class="col-sm-6">
						<table class="table table-condensed table-hover">
							<thead>
								<tr>
									<th colspan="3">Data Karyawan</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td width="20%">NIP</td>
									<td>: </td>
									<td><input type="text" name="nip2" value="<?php echo $row->nip;?>" class="form-select" readonly=""></td>
								</tr>
								<tr>
									<td>Nama</td>
									<td>: </td>
									<td><input type="text" name="nama" value="<?php echo $row->nama;?>" class="form-select" readonly="" style="width: 100%;"></td>
								</tr>
								<tr>
									<td>Unit</td>
									<td>: </td>
									<td><input type="text" name="unit" value="<?php echo $row->unit;?>" class="form-select" readonly="" style="width: 100%;"></td>
								</tr>
								<tr>
									<td>Penempatan</td>
									<td>: </td>
									<td><input type="text" name="jabatan" value="<?php echo $row->jabatan;?>" class="form-select" readonly="" style="width: 100%;"></td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="col-sm-6">
						<table class="table table-condensed table-hover">
							<thead>
								<tr>
									<th colspan="3">Data Penempatan</th>
								</tr>
							</thead>
							<tbody>
								<?php 
								$data_unit = $this->db->query("SELECT * FROM online_master_penempatan_karyawan where id_record_karyawan='$row->id_record'")->result();
								foreach ($data_unit as $key) {
									$dunit = $this->db->query("SELECT * FROM online_master_unit where id_record = '$key->id_record_unit'")->row();
									?>
									<tr>
										<td><input type="text" value="<?php echo $dunit->unit;?>" class="form-select" readonly="" style="width: 100%;"></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>	
			<div class="modal-footer">
				<input type="hidden" name="id_record_karyawan" value="<?php echo $row->id_record;?>" class="form-control">
				<button type="button" data-dismiss="modal" class="btn btn-default">Batal</button>
				<button type="submit" class="btn btn-primary">Tambah</button>
			</div>	
		</form>
	</div>

	<div id="hapus<?php echo $row->id_record;?>" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
		<div class="modal-body">
			<?php $data = $this->db->query("SELECT * FROM online_master_karyawan where id_record = '$row->id_record'")->row_array();?>
			Hapus data karyawan atas nama <strong><?php echo $data['nama'];?> </strong>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-default"> Batal </button>
			<button type="button" data-dismiss="modal" class="btn btn-danger" onclick="location.href='<?php echo base_url();?>admin/karyawan/hapus/<?php echo $row->id_record;?>';"> Hapus </button>
		</div>
	</div>

<?php endforeach; ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script>
	$(document).ready(function() {
		$('#cari').click(function() {
			var nip = $('#nip').val();  
			if(nip != '') {
				$.ajax({
					url:"<?php echo base_url();?>admin/karyawan/cari_nip",
					method:"POST",
					data:{nip:nip},
					success:function(data) {
						let da = JSON.parse(data)
						// console.log(da.nip)

						$('#nip2').val(da.nip);
						$('#nama').val(da.nama); 
						$('#unit').val(da.unit); 
						$('#jabatan').val(da.penempatan);

						if(da.nip=="Data kosong"){
							document.getElementById('submit').style.display = 'none';
						}else{
							if(da.alert=="oke"){
								document.getElementById('submit').style.display = 'block';
							}else{								
								alert(da.alert);
								document.getElementById('submit').style.display = 'none';
							}
						}
						 
					}
				});
			}
		});
	});
</script>