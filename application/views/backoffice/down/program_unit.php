<div class="row">
	<div class="col-sm-12">
		<div class="page-header">
			<h1>Data Master Program Unit</h1>
		</div>
	</div>
</div>

<?php 
	$pilih = $this->db->query("SELECT * FROM online_master_unit")->result();
	$url_back = base_url(uri_string());
?>

<div class="row">
	<div class="col-sm-12">
		<form method="post" role="form" action="<?php echo base_url();?>admin/master/pilih_unit">
			<label>
				Silahkan Pilih Unit:
				<select name="id_record_unit" id="unit" class="form-control search-select" required style="width: 400px;">
					<option value="">-- Pilih Unit --</option>
					<?php foreach ($pilih as $unit) { ?>
						<option value="<?php echo $unit->id_record; ?>"><?php echo $unit->unit; ?></option>
					<?php }; ?>
				</select>
			</label>
			<input type="hidden" name="url" value="program_unit">
			<button type="submit" id="cari" class="btn btn-primary" style="margin-left: 30px">
				Pilih
			</button>
		</form>
	</div>
</div>

<br>

<div class="row">
	<?php if(empty($id_unit)){ ?>
		<div class="col-md-9 mb-3 mb-md-0">
			<a href="#" data-toggle="modal" data-target="#tambah-data-program" class="btn btn-sm btn-primary">
				<i class="glyphicon glyphicon-plus"></i> Tambah Data
			</a>
		</div>
	<?php }else{ ?>
		<div class="col-md-9 mb-3 mb-md-0">
			<a href="#" data-toggle="modal" data-target="#tambah-data" class="btn btn-sm btn-primary">
				<i class="glyphicon glyphicon-plus"></i> Tambah Data
			</a>
		</div>
	<?php } ?>
</div>

<br>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<i class="fa fa-external-link-square"></i>
				Data Master Program Unit
				<?php if(empty($id_unit)){

				}else{ 
					$dunit = $this->db->query("SELECT * FROM online_master_unit where id_record='$id_unit'")->row();
					echo '<strong>'.$dunit->unit.'</strong>';
				} ?>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover" id="sample-table-2">
						<thead>
							<tr>
								<th class="center">NO</th>
								<th>Unit</th>
								<th>Sasaran mutu</th>
								<th>Indikator</th>
								<th>Target</th>
								<th>Pencapaian</th>
								<th>Keterangan</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php if (empty($data)) { ?>
							<tr><td colspan="12" style="text-align: center; font-weight: bold">-- Tidak ada program unit --</td></tr>
							<?php } else { ?>
							<?php $nomor=0; foreach ($data as $row): ?>
							<tr>
								<td class="text-center">
									<?php echo $nomor=$nomor+1; ?>
								</td>
								<td>
									<?php
									$dunit = $this->db->query("SELECT * FROM online_master_unit where id_record = '$row->id_record_unit'")->row_array();
									echo $dunit['unit'];
									?>
								</td>
								<td>
									<?php echo $row->sarmut; ?>
								</td>
								<td>
									<?php echo $row->indikator; ?>
								</td>
								<td>
									<?php echo $row->target; ?>
								</td>
								<td>
									<?php echo $row->pencapaian; ?>
								</td>
								<td>
									<?php echo $row->keterangan; ?>
								</td>
								<td>
									<a href="#edit<?php echo $row->id;?>" data-toggle="modal" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
									<a href="#hapus<?php echo $row->id;?>" data-toggle="modal" class="btn btn-xs btn-bricky tooltips" data-placement="top" data-original-title="Hapus"><i class="fa fa-times fa fa-white"></i></a>
								</td>
							</tr>
							<?php endforeach; ?>
							<?php }; ?>
						</tbody>
					</table>
					<div class="text-center mt-3">
						<?php echo $this->pagination->create_links(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php foreach ($data as $row):?>
	<div id="edit<?php echo $row->id;?>" class="modal fade" data-width="760" style="display: none;">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				&times;
			</button>
			<h4 class="modal-title">Edit Program Unit</h4>
		</div>
		<form method="post" role="form" action="<?php echo base_url();?>admin/master/edit_data_program">
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label>Sasaran Mutu</label>
							<input type="text" value="<?php echo $row->sarmut;?>" name="sasaran_mutu" placeholder="Sasaran Mutu" class="form-control" autocomplete="off" required>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Indikator</label>
							<input type="text" value="<?php echo $row->indikator;?>" name="indikator" placeholder="Indikator" class="form-control" autocomplete="off" required>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Target</label>
							<input type="text" value="<?php echo $row->target;?>" name="target" placeholder="Target" autocomplete="off" class="form-control">
						</div>
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<label>Pencapaian</label>
							<input type="text" value="<?php echo $row->pencapaian;?>" name="pencapaian" placeholder="Pencapaian" autocomplete="off" class="form-control">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Terealisasi ??</label>
							<div>
								<label class="radio-inline">
									<input type="radio" class="grey" value="Y" name="status_program" <?php echo ($row->status_program == 'Y') ? "checked" : ""; ?>>
									Ya
								</label>
								<label class="radio-inline">
									<input type="radio" class="grey" value="T" name="status_program" <?php echo ($row->status_program == 'T') ? "checked" : ""; ?>>
									Tidak
								</label>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Keterangan</label>
							<textarea name="keterangan" class="form-control"><?php echo $row->sarmut;?></textarea>
						</div>
					</div>
				</div>
			</div>	
			<div class="modal-footer">
				<input type="hidden" name="id" value="<?php echo $row->id;?>" class="form-control">
				<input type="hidden" name="id_record_unit" value="<?php echo $id_unit;?>" class="form-control">
				<button type="button" data-dismiss="modal" class="btn btn-default">Batal</button>
				<button type="submit" class="btn btn-primary">Tambah</button>
			</div>	
		</form>
	</div>

	<div id="hapus<?php echo $row->id;?>" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
		<div class="modal-body">
			Hapus data Program Unit
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-default"> Batal </button>
			<button type="button" data-dismiss="modal" class="btn btn-danger" onclick="location.href='<?php echo base_url();?>admin/master/hapus_program/<?php echo $row->id;?>/<?php echo $row->id_record_unit;?>';"> Hapus </button>
		</div>
	</div>
<?php endforeach;?>


<div id="tambah-data" class="modal fade" data-width="760" style="display: none;">
	<?php $dat = $this->db->query("SELECT * FROM online_master_unit WHERE id_record = '$id_unit'")->row();?>
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
			&times;
		</button>
		<h4 class="modal-title">Tambah Data Program Unit <strong><?php echo $dat->unit;?></strong></h4>
	</div>
	<form method="post" role="form" action="<?php echo base_url();?>admin/master/tambah_data_program">
		<div class="modal-body">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>Sasaran Mutu</label>
						<input type="text" name="sasaran_mutu" placeholder="Sasaran Mutu" class="form-control" autocomplete="off" required>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label>Indikator</label>
						<input type="text" name="indikator" placeholder="Indikator" class="form-control" autocomplete="off" required>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Target</label>
						<input type="text" name="target" placeholder="Target" autocomplete="off" class="form-control">
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label>Pencapaian</label>
						<input type="text" name="pencapaian" placeholder="Pencapaian" autocomplete="off" class="form-control">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Terealisasi ??</label>
						<div>
							<label class="radio-inline">
								<input type="radio" class="grey" value="Y" name="status_program">
								Ya
							</label>
							<label class="radio-inline">
								<input type="radio" class="grey" value="T" name="status_program">
								Tidak
							</label>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label>Keterangan</label>
						<textarea name="keterangan" class="form-control"></textarea>
					</div>
				</div>
			</div>
		</div>	
		<div class="modal-footer">
			<input type="hidden" name="id_record_unit" value="<?php echo $id_unit;?>" class="form-control">
			<button type="button" data-dismiss="modal" class="btn btn-default">Batal</button>
			<button type="submit" class="btn btn-primary">Tambah</button>
		</div>	
	</form>
</div>

<div id="tambah-data-program" class="modal fade" data-width="760" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
			&times;
		</button>
		<h4 class="modal-title">Tambah Data</h4>
	</div>
	<form method="post" role="form" action="<?php echo base_url();?>admin/master/tambah_data_program">
		<div class="modal-body">
			<div class="row">				
				<div class="col-md-12">
					<div class="form-group">
						<label>Pilih Unit</label>
						<select name="id_record_unit" class="form-control search-select" required>
							<option value="">-- Pilih Unit --</option>
							<?php foreach ($pilih as $un) { ?>
								<option value="<?php echo $un->id_record; ?>"><?php echo $un->unit; ?></option>
							<?php }; ?>
						</select>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label>Sasaran Mutu</label>
						<input type="text" name="sasaran_mutu" placeholder="Sasaran Mutu" class="form-control" autocomplete="off" required>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label>Indikator</label>
						<input type="text" name="indikator" placeholder="Indikator" class="form-control" autocomplete="off" required>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label>Target</label>
						<input type="text" name="target" placeholder="Target" autocomplete="off" class="form-control">
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label>Pencapaian</label>
						<input type="text" name="pencapaian" placeholder="Pencapaian" autocomplete="off" class="form-control">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Terealisasi ??</label>
						<div>
							<label class="radio-inline">
								<input type="radio" class="grey" value="Y" name="status_program">
								Ya
							</label>
							<label class="radio-inline">
								<input type="radio" class="grey" value="T" name="status_program">
								Tidak
							</label>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label>Keterangan</label>
						<textarea name="keterangan" class="form-control"></textarea>
					</div>
				</div>
			</div>
		</div>	
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-default">Batal</button>
			<button type="submit" class="btn btn-primary">Tambah</button>
		</div>	
	</form>
</div>