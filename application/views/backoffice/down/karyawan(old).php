<div class="row">
	<div class="col-sm-12">
		<div class="page-header">
			<h1><?php echo $title;?></h1>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-9 mb-3 mb-md-0">
		<a href="#tambah" data-toggle="modal" class="btn btn-sm btn-primary">
			<i class="glyphicon glyphicon-plus"></i> Tambah Karyawan
		</a>
	</div>
	<div class="col-md-3 pull-right">
		<form method="POST" action="<?php echo site_url('pesan/search');?>">
			<div class="input-group">
				<input type="text" name="search" onkeyup="pencarian_tabel()" class="form-control" placeholder="Search..." autocomplete="off">
				<span class="input-group-btn">
					<button class="btn btn-primary" type="submit"> <i class="fa fa-search"></i> </button>
				</span>
			</div>
		</form>
	</div>
</div>

<br>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<i class="fa fa-external-link-square"></i>
				<?php echo $description;?>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover" id="sample-table-2">
						<thead>
							<tr>
								<th class="center">NO</th>
								<th>Nama</th>
								<th>Unit</th>
								<th>Penempatan</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php if (empty($karyawan)) { ?>
							<tr><td colspan="12" style="text-align: center; font-weight: bold">-- Data karyawan kosong --</td></tr>
							<?php } else { ?>
							<?php $nomor=0; foreach ($karyawan as $row): ?>
							<tr>
								<td class="text-center">
									<?php echo $nomor=$nomor+1; ?>
								</td>
								<td>
									<?php echo $row->nama; ?>
								</td>
								<td>
									<?php echo $row->unit; ?>
								</td>
								<td> - </td>
								<td class="center">
									<a href="#edit<?php echo $row->id_record;?>" data-toggle="modal" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
									<a href="#hapus<?php echo $row->id_record;?>" data-toggle="modal" class="btn btn-xs btn-bricky tooltips" data-placement="top" data-original-title="Hapus"><i class="fa fa-times fa fa-white"></i></a>
								</td>
							</tr>
							<?php endforeach; ?>
							<?php }; ?>
						</tbody>
					</table>
					<div class="text-center mt-3">
						<?php echo $this->pagination->create_links(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="tambah" class="modal fade" data-focus-on="input:first" style="display: none;">
	<?php $data = $this->db->query("SELECT * FROM online_master_karyawan order by id_record asc")->result(); ?>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					&times;
				</button>
				<h4 class="modal-title">Tambah Karyawan</h4>
			</div>
			<form method="post" role="form" class="form-horizontal" action="<?php echo base_url();?>karyawan/tambah_karyawan">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-4">
						<label>NIP</label>
						<p>
							<select name="dari" class="form-control search-select" required="" onchange="changeValue(this.value)">
								<option value=" ">-- Pilih Karyawan --</option>
								<?php $jsArray = "var Ddata = new Array();\n";?>
								<?php foreach ($data as $un) { ?>
								<option value="<?php echo $un->id_record; ?>"><?php echo $un->nip; ?></option>

								<?php $jsArray .= 
								"Ddata[	'".$un->id_record ."'] = {
									nip:	'".$un->nip ."',
									nama:	'".$un->nama ."',
									unit:	'".$un->unit ."',
									jabatan:'".$un->jabatan ."'
								};\n";
								?>

								<?php }; ?>
							</select>
						</p>
						</div>

						<div class="col-sm-7 col-md-offset-1">
							<table class="table table-condensed table-hover">
								<thead>
									<tr>
										<th colspan="3">Data Karyawan</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td width="20%">NIP</td>
										<td>: <input type="text" id="nip" class="form-select" disabled=""></td>
									</tr>
									<tr>
										<td>Nama</td>
										<td>: <input type="text" id="nama" class="form-select" disabled=""></td>
									</tr>
									<tr>
										<td>Unit</td>
										<td>: <input type="text" id="unit" class="form-select" disabled=""></td>
									</tr>
									<tr>
										<td>Penempatan</td>
										<td>: <input type="text" id="jabatan" class="form-select" disabled=""></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" data-dismiss="modal" class="btn btn-default">Batal</button>
					<button type="submit" class="btn btn-primary">Tambah</button>
				</div>
			</form>
		</div>
	</div>
</div>

<?php foreach ($karyawan as $row): ?>
<div id="edit<?php echo $row->id_record;?>" class="modal fade" tabindex="-1" data-focus-on="input:first" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					&times;
				</button>
				<h4 class="modal-title">Tambah Karyawan</h4>
			</div>
			<form method="post" role="form" class="form-horizontal" action="<?php echo base_url();?>karyawan/tambah_karyawan">
				<div class="modal-body">
					
				</div>
				<div class="modal-footer">
					<button type="button" data-dismiss="modal" class="btn btn-default">Batal</button>
					<button type="submit" class="btn btn-primary">Tambah</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div id="hapus<?php echo $row->id_record;?>" class="modal fade" data-keyboard="false" style="display: none;">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
				<?php $data = $this->db->query("SELECT * FROM online_master_karyawan where id_record = '$row->id_record'")->row_array();?>
				Hapus data karyawan atas nama <strong><?php echo $data['nama'];?> </strong>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-default"> Batal </button>
				<button type="button" data-dismiss="modal" class="btn btn-danger" onclick="location.href='<?php echo base_url();?>admin/karyawan/hapus/<?php echo $row->id_record;?>';"> Hapus </button>
			</div>
		</div>
	</div>
</div>

<?php endforeach; ?>

<script type="text/javascript">    
	<?php echo $jsArray; ?>  
	function changeValue(x){  
		document.getElementById('nip').value = Ddata[x].nip; 
		document.getElementById('nama').value = Ddata[x].nama;  
		document.getElementById('unit').value = Ddata[x].unit;   
		document.getElementById('jabatan').value = Ddata[x].jabatan;
	};  
</script> 