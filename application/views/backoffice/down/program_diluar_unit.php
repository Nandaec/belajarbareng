<div class="row">
	<div class="col-sm-12">
		<div class="page-header">
			<h1>Data Master Kegiatan Diluar Program </h1>
		</div>
	</div>
</div>

<?php 
	$pilih = $this->db->query("SELECT * FROM online_master_unit")->result();
	$url_back = base_url(uri_string());
?>

<div class="row">
	<div class="col-sm-12">
		<form method="post" role="form" action="<?php echo base_url();?>admin/master/pilih_unit">
			<label>
				Silahkan Pilih Unit:
				<select name="id_record_unit" id="unit" class="form-control search-select" required style="width: 400px;">
					<option value="">-- Pilih Unit --</option>
					<?php foreach ($pilih as $unit) { ?>
						<option value="<?php echo $unit->id_record; ?>"><?php echo $unit->unit; ?></option>
					<?php }; ?>
				</select>
			</label>
			<input type="hidden" name="url" value="program_diluar_unit">
			<button type="submit" id="cari" class="btn btn-primary" style="margin-left: 30px">
				Pilih
			</button>
		</form>
	</div>
</div>

<br>

<div class="row">
	<?php if(empty($id_unit)){ ?>
		<div class="col-md-9 mb-3 mb-md-0">
			<a href="#" data-toggle="modal" data-target="#tambah-data-program" class="btn btn-sm btn-primary">
				<i class="glyphicon glyphicon-plus"></i> Tambah Data
			</a>
		</div>
	<?php }else{ ?>
		<div class="col-md-9 mb-3 mb-md-0">
			<a href="#" data-toggle="modal" data-target="#tambah-data" class="btn btn-sm btn-primary">
				<i class="glyphicon glyphicon-plus"></i> Tambah Data
			</a>
		</div>
	<?php } ?>
</div>

<br>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<i class="fa fa-external-link-square"></i>
				Data Master Kegiatan Diluar Unit
				<?php if(empty($id_unit)){

				}else{ 
					$dunit = $this->db->query("SELECT * FROM online_master_unit where id_record='$id_unit'")->row();
					echo '<strong>'.$dunit->unit.'</strong>';
				} ?>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover" id="sample-table-2">
						<thead>
							<tr>
								<th class="center">NO</th>
								<th>Unit</th>
								<th>Program</th>
								<th>Urgensi</th>
								<th>Pencapaian</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php if (empty($data)) { ?>
							<tr><td colspan="12" style="text-align: center; font-weight: bold">-- Tidak ada program unit --</td></tr>
							<?php } else { ?>
							<?php $nomor=0; foreach ($data as $row): ?>
							<tr>
								<td class="text-center">
									<?php echo $nomor=$nomor+1; ?>
								</td>
								<td>
									<?php
									$dunit = $this->db->query("SELECT * FROM online_master_unit where id_record = '$row->id_record_unit'")->row_array();
									echo $dunit['unit'];
									?>
								</td>
								<td>
									<?php echo $row->program; ?>
								</td>
								<td>
									<?php echo $row->urgensi; ?>
								</td>
								<td>
									<?php echo $row->pencapaian; ?>
								</td>
								<td>
									<a href="#edit<?php echo $row->id;?>" data-toggle="modal" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
									<a href="#hapus<?php echo $row->id;?>" data-toggle="modal" class="btn btn-xs btn-bricky tooltips" data-placement="top" data-original-title="Hapus"><i class="fa fa-times fa fa-white"></i></a>
								</td>
							</tr>
							<?php endforeach; ?>
							<?php }; ?>
						</tbody>
					</table>
					<div class="text-center mt-3">
						<?php echo $this->pagination->create_links(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php foreach ($data as $row):?>
	<div id="edit<?php echo $row->id;?>" class="modal fade" data-width="760" style="display: none;">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				&times;
			</button>
			<h4 class="modal-title">Edit Program Unit</h4>
		</div>
		<form method="post" role="form" action="<?php echo base_url();?>admin/master/edit_program_diluar_unit">
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label>Program</label>
							<input type="text" value="<?php echo $row->program;?>" name="program" placeholder="Program" class="form-control" autocomplete="off" required>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Urgensi</label>
							<input type="text" value="<?php echo $row->urgensi;?>" name="urgensi" placeholder="Urgensi" class="form-control" autocomplete="off" required>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<label>Pencapaian</label>
							<input type="text" value="<?php echo $row->pencapaian;?>" name="pencapaian" placeholder="Pencapaian" autocomplete="off" class="form-control">
						</div>
					</div>
				</div>
			</div>	
			<div class="modal-footer">
				<input type="hidden" name="id" value="<?php echo $row->id;?>" class="form-control">
				<input type="hidden" name="id_record_unit" value="<?php echo $id_unit;?>" class="form-control">
				<button type="button" data-dismiss="modal" class="btn btn-default">Batal</button>
				<button type="submit" class="btn btn-primary">Tambah</button>
			</div>	
		</form>
	</div>

	<div id="hapus<?php echo $row->id;?>" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
		<div class="modal-body">
			Hapus data kegiatan diluar unit
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-default"> Batal </button>
			<button type="button" data-dismiss="modal" class="btn btn-danger" onclick="location.href='<?php echo base_url();?>admin/master/hapus_program_diluar_unit/<?php echo $row->id;?>/<?php echo $row->id_record_unit;?>';"> Hapus </button>
		</div>
	</div>
<?php endforeach;?>


<div id="tambah-data" class="modal fade" data-width="760" style="display: none;">
	<?php $dat = $this->db->query("SELECT * FROM online_master_unit WHERE id_record = '$id_unit'")->row();?>
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
			&times;
		</button>
		<h4 class="modal-title">Tambah Data Kegiatan Diluar Program Unit <strong><?php echo $dat->unit;?></strong></h4>
	</div>
	<form method="post" role="form" action="<?php echo base_url();?>admin/master/tambah_program_diluar_unit">
		<div class="modal-body">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>Program</label>
						<input type="text" name="program" placeholder="Program" class="form-control" autocomplete="off" required>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label>Urgensi</label>
						<input type="text" name="urgensi" placeholder="Urgensi" class="form-control" autocomplete="off" required>
					</div>
				</div>

				<div class="col-md-12">
					<div class="form-group">
						<label>Pencapaian</label>
						<input type="text" name="pencapaian" placeholder="Pencapaian" autocomplete="off" class="form-control">
					</div>
				</div>
			</div>
		</div>	
		<div class="modal-footer">
			<input type="hidden" name="id_record_unit" value="<?php echo $id_unit;?>" class="form-control">
			<button type="button" data-dismiss="modal" class="btn btn-default">Batal</button>
			<button type="submit" class="btn btn-primary">Tambah</button>
		</div>	
	</form>
</div>

<div id="tambah-data-program" class="modal fade" data-width="760" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
			&times;
		</button>
		<h4 class="modal-title">Tambah Data Kegiatan Diluar Program Unit</h4>
	</div>
	<form method="post" role="form" action="<?php echo base_url();?>admin/master/tambah_program_diluar_unit">
		<div class="modal-body">
			<div class="row">				
				<div class="col-md-12">
					<div class="form-group">
						<label>Pilih Unit</label>
						<select name="id_record_unit" class="form-control search-select" required>
							<option value="">-- Pilih Unit --</option>
							<?php foreach ($pilih as $un) { ?>
								<option value="<?php echo $un->id_record; ?>"><?php echo $un->unit; ?></option>
							<?php }; ?>
						</select>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label>Program</label>
						<input type="text" name="program" placeholder="Program" class="form-control" autocomplete="off" required>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label>Urgensi</label>
						<input type="text" name="urgensi" placeholder="Urgensi" class="form-control" autocomplete="off" required>
					</div>
				</div>


				<div class="col-md-12">
					<div class="form-group">
						<label>Pencapaian</label>
						<input type="text" name="pencapaian" placeholder="Pencapaian" autocomplete="off" class="form-control">
					</div>
				</div>
			</div>
		</div>	
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-default">Batal</button>
			<button type="submit" class="btn btn-primary">Tambah</button>
		</div>	
	</form>
</div>