<div class="row">
	<div class="col-sm-12">
		<div class="page-header">
			<h1><?php echo $title;?></h1>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-3 pull-right">
		<form method="POST" action="<?php echo site_url('admin/proses_disposisi/search');?>">
			<div class="input-group">
				<input type="text" name="search" onkeyup="pencarian_tabel()" class="form-control" placeholder="Pencarian" autocomplete="off">
				<span class="input-group-btn">
					<button class="btn btn-primary" type="submit"> <i class="fa fa-search"></i> </button>
				</span>
			</div>
		</form>
	</div>
</div>

<br>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<i class="fa fa-external-link-square"></i>
				<?php echo $description;?>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover" id="sample-table-2">
						<thead>
							<tr>
								<th class="center">NO</th>
								<th>Judul Surat</th>
								<th>Pengirim</th>
								<th>Unit Pengirim</th>
								<th>Kepada Unit</th>
								<th>Tanggal</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php if (empty($proses_disposisi)) { ?>
							<tr><td colspan="12" style="text-align: center; font-weight: bold">-- Tidak ada surat --</td></tr>
							<?php } else { ?>
							<?php $nomor=0; foreach ($proses_disposisi as $row): ?>
							<tr>
								<td class="text-center">
									<?php echo $nomor=$nomor+1; ?>
								</td>
								<td>
									<?php
									$osd = $this->db->query("SELECT * FROM online_surat_disposisi where id_record = '$row->id_record_surat_disposisi'")->row_array();
									echo ucwords($osd['judul']);
									?>
								</td>
								<td>
									<a href="<?php echo base_url();?>pesan/pdf/<?php echo $row->id_record_surat_disposisi;?>" rel="nofollow">
										<?php $dunit = $this->db->query("SELECT * FROM online_master_karyawan where id_record = '$row->id_record_karyawan_from'")->row_array();
										echo $dunit['nama']; ?>
									</a>
									<?php if($row->status_kirim_oleh_sekret=='f'){ ?>
										<span class="badge badge-new"> new </span>
									<?php }else{ ?>

									<?php } ?>
								</td>
								<td>
									<?php
									$dunit = $this->db->query("SELECT * FROM online_master_unit where id_record = '$row->id_record_unit_from'")->row_array();
									echo $dunit['unit'];
									?>
								</td>
								<td>
									<?php
									$dunit = $this->db->query("SELECT * FROM online_master_unit where id_record = '$row->id_record_unit_to'")->row_array();
									echo $dunit['unit'];
									?>
								</td>
								<td><?php echo $row->date_kirim_oleh_sekret;?></td>
								<!-- <td>
									<?php if($row->ket==1){ ?>
										<span class="label label-warning"> Proses</span>
									<?php }elseif($row->ket==2){ ?>
										<span class="label label-success"> Selesai</span>
									<?php }else{ ?>
										<span class="label label-danger"> Batal</span>
									<?php } ?>
								</td> -->
								<td class="center">
									<?php $status_kirim = $row->status_kirim_oleh_sekret;
									if($status_kirim=='f'){ ?>
										<a href="#kirim<?php echo $row->id_record_surat_disposisi;?>" data-toggle="modal" class="btn btn-xs btn-warning tooltips" data-original-title="Kirim">Kirim</a>
										<a href="#" class="btn btn-xs btn-danger tooltips" data-original-title="Hapus"><i class="fa fa-mail-reply"></i></a>
									<?php }else{ ?>
										<span class="label label-success"> Sukses</span>
									<?php } ?>
								</td>
							</tr>
							<?php endforeach; ?>
							<?php }; ?>
						</tbody>
					</table>
					<div class="text-center mt-3">
						<?php echo $this->pagination->create_links(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php foreach ($proses_disposisi as $row): ?>
	<div id="kirim<?php echo $row->id_record_surat_disposisi;?>" class="modal fade" data-keyboard="false" style="display: none;">
		<div class="modal-body">
			<p>
				<?php $dari = $this->db->query("SELECT * FROM online_master_unit where id_record = '$row->id_record_unit_from'")->row_array(); ?>
				<?php $to = $this->db->query("SELECT * FROM online_master_unit where id_record = '$row->id_record_unit_to'")->row_array(); ?>

				Anda akan mengirimkan surat 
				dari <strong> <?php echo $dari['unit'];?> </strong> 
				kepada <strong><?php echo $to['unit'];?></strong>
				??
			</p>
		</div>
		<div class="modal-footer">		
			<form method="POST" action="<?php echo site_url('admin/proses_disposisi/kirim');?>">	
				<input type="hidden" name="id_record_surat_disposisi" value="<?php echo $row->id_record_surat_disposisi;?>">	
				<input type="hidden" name="unit_dari" value="<?php echo $dari['unit'];?>">
				<input type="hidden" name="unit_kepada" value="<?php echo $to['unit'];?>">
				<button type="button" data-dismiss="modal" class="btn btn-default"> Batal </button>
				<button type="submit" class="btn btn-primary"> Kirim </button>
			</form>
		</div>
	</div>

<?php endforeach; ?>

<!-- <div id="long" class="modal fade" tabindex="-1" data-replace="true" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
			&times;
		</button>
		<h4 class="modal-title">A Fairly Long Modal</h4>
	</div>
	<div class="modal-body">
		
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">
			Close
		</button>
	</div>
</div> -->