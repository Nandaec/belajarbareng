<div class="box-login">
    <div class="text-center" style="margin-bottom: 10px;">
    <h3>Halaman Login Admin</h3>
    <!-- <img src="<?php echo base_url();?>assets/backoffice/images/logo.png" width="25%" height="25%"> -->
    </div>

    <?php if ($this->session->flashdata('callout-success') == TRUE) {; ?>
    <div class="alert alert-success" id="slide_slow">
        <p><?php echo $this->session->flashdata('callout-success'); ?></p>
    </div>
    <?php } if ($this->session->flashdata('callout-danger') == TRUE) {; ?>
    <div class="alert alert-danger" id="slide_slow"> <!-- <h4>I am a danger callout!</h4> -->
        <p><?php echo $this->session->flashdata('callout-danger'); ?></p>
    </div>
    <?php } if ($this->session->flashdata('callout-info') == TRUE) {; ?>
    <div class="alert alert-info" id="slide_slow">
        <p><?php echo $this->session->flashdata('callout-info'); ?></p>
    </div>
    <?php } if ($this->session->flashdata('callout-warning') == TRUE) {; ?>
    <div class="alert alert-warning" id="slide_slow">
        <p><?php echo $this->session->flashdata('callout-warning'); ?></p>
    </div>
    <?php }; ?>

    <form method="post" action="<?php echo site_url('backoffice/login'); ?>" class="form-login">
        <fieldset>
            <div class="form-group">
                <span class="input-icon">
                    <input type="text" class="form-control" name="username" placeholder="Username">
                    <i class="fa fa-user"></i>
                </span>
            </div>
            <div class="form-group form-actions">
                <span class="input-icon">
                    <input type="password" class="form-control password" name="password" placeholder="Password">
                    <i class="fa fa-lock"></i>
                </span>
            </div>
            <div class="form-actions">
                <button type="submit" name="login" class="form-control btn btn-biru pull-right">
                    Masuk <!-- <i class="fa fa-arrow-circle-right"></i> -->
                </button>
            </div>
        </fieldset>
    </form>
</div>