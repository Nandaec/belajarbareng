<div class="row">
	<div class="col-sm-12">
		<div class="page-header">
			<h1>Data Administrator</h1>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-9 mb-3 mb-md-0">
		<a href="#" data-toggle="modal" data-target="#tambah-data" class="btn btn-sm btn-primary">
			<i class="glyphicon glyphicon-plus"></i> Tambah Data
		</a>
	</div>
</div>

<br>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<i class="fa fa-external-link-square"></i>
				Data Administrator
			</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover" id="sample-table-2">
						<thead>
							<tr>
								<th class="center">NO</th>
								<th>ID Admin</th>
								<th>Username</th>
								<th>Nama</th>
								<th>Email</th>
								<th>No.Hp</th>
								<th>Tanggal Gabung</th>
								<th>Level</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php if (empty($administrator)) { ?>
							<tr><td colspan="12" style="text-align: center; font-weight: bold">-- Tidak ada sasaran mutu --</td></tr>
							<?php } else { ?>
							<?php $nomor=0; foreach ($administrator as $row): ?>
							<tr>
								<td class="text-center">
									<?php echo $nomor=$nomor+1; ?>
								</td>
								<td>
									<?php echo $row->id_admin; ?>
								</td>
								<td>
									<?php echo $row->username; ?>
								</td>
								<td>
									<?php echo $row->nama; ?>
								</td>
								<td>
									<?php echo $row->email; ?>
								</td>
								<td>
									<?php echo $row->phone; ?>
								</td>
								<td>
									<?php echo $row->tanggal_register; ?>
								</td>
								<td>
									<?php echo $row->level; ?>
								</td>
								<td>
									<a href="#edit<?php echo $row->id_admin;?>" data-toggle="modal" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
									<a href="#hapus<?php echo $row->id_admin;?>" data-toggle="modal" class="btn btn-xs btn-bricky tooltips" data-placement="top" data-original-title="Hapus"><i class="fa fa-times fa fa-white"></i></a>
								</td>
							</tr>
							<?php endforeach; ?>
							<?php }; ?>
						</tbody>
					</table>
					<div class="text-center mt-3">
						<?php echo $this->pagination->create_links(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php foreach ($administrator as $row):?>
	<div id="edit<?php echo $row->id_admin;?>" class="modal fade" data-width="760" style="display: none;">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				&times;
			</button>
			<h4 class="modal-title">Edit Sasaran Mutu</h4>
		</div>
		<form method="post" role="form" action="<?php echo base_url();?>backoffice/administrator/edit">
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>ID Admin</label>
							<input type="text" name="id_admin" value="<?php echo $row->id_admin;?>" class="form-control" readonly="">
						</div>
					</div>	
					<div class="col-md-6">
						<div class="form-group">
							<label>Username</label>
							<input type="text" name="username" value="<?php echo $row->username;?>" placeholder="Username" class="form-control" autocomplete="off" required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Email</label>
							<input type="text" name="email" value="<?php echo $row->email;?>" placeholder="Email" class="form-control" autocomplete="off" required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Nama</label>
							<input type="text" name="nama" value="<?php echo $row->nama;?>" placeholder="Nama" autocomplete="off" class="form-control">
						</div>
					</div>			
					<div class="col-md-4">
						<div class="form-group">
							<label>Alamat</label>
							<input type="text" name="alamat" value="<?php echo $row->alamat;?>" placeholder="Alamat" autocomplete="off" class="form-control">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Tempat Lahir</label>
							<input type="text" name="tempat_lahir" value="<?php echo $row->tempat_lahir;?>" placeholder="Tempate Lahir" autocomplete="off" class="form-control">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Tanggak Lahir</label>
							<input type="text" name="tanggal_lahir" value="<?php echo $row->tanggal_lahir;?>" id="datepicker" data-date-format="yyyy-mm-dd" data-date-viewmode="years" class="form-control date-picker">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>No. Hp</label>
							<input type="text" name="phone" value="<?php echo $row->phone;?>" placeholder="No. Hp" autocomplete="off" class="form-control">
						</div>
					</div>	
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">Jenis Kelamin</label>
							<div>
								<label class="radio-inline">
									<input type="radio" class="grey" value="L" name="jenis_kelamin" <?php echo ($row->jenis_kelamin == 'L') ? "checked" : ""; ?> id="gender_female">
									Laki-laki
								</label>
								<label class="radio-inline">
									<input type="radio" class="grey" value="P" name="jenis_kelamin" <?php echo ($row->jenis_kelamin == 'P') ? "checked" : ""; ?>  id="gender_male">
									Perempuan
								</label>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Level</label>
							<?php $level = $this->db->query("SELECT * from p_level order by id_level ASC")->result(); ?>
							<select name="level" id="form-field-select-3" class="form-control select" required="">
								<option value="">-- Pilih Level --</option>
								<?php foreach ($level as $lev) { ?>
								<option value="<?php echo $lev->id_level; ?>" <?php echo ($lev->id_level == $row->level) ? "selected" : ""; ?>><?php echo $lev->level; ?></option>
								<?php }; ?>
							</select>
						</div>
					</div>
				</div>
			</div>	
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-default">Batal</button>
				<button type="submit" class="btn btn-primary">Tambah</button>
			</div>	
		</form>
	</div>

	<div id="hapus<?php echo $row->id_admin;?>" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
		<div class="modal-body">
			Hapus data Administrator
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-default"> Batal </button>
			<button type="button" data-dismiss="modal" class="btn btn-danger" onclick="location.href='<?php echo base_url();?>backoffice/administrator/hapus/<?php echo $row->id_admin;?>';"> Hapus </button>
		</div>
	</div>
<?php endforeach;?>

<div id="tambah-data" class="modal fade" data-width="760" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
			&times;
		</button>
		<h4 class="modal-title">Tambah Data Administrator</h4>
	</div>
	<form method="post" role="form" action="<?php echo base_url();?>backoffice/administrator/tambah">
		<div class="modal-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>ID Admin</label>
						<input type="text" name="id_admin" value="<?php echo $id_admin;?>" class="form-control" readonly="">
					</div>
				</div>	
				<div class="col-md-6">
					<div class="form-group">
						<label>Username</label>
						<input type="text" name="username" placeholder="Username" class="form-control" autocomplete="off" required>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>Email</label>
						<input type="text" name="email" placeholder="Email" class="form-control" autocomplete="off" required>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>Nama</label>
						<input type="text" name="nama" placeholder="Nama" autocomplete="off" class="form-control">
					</div>
				</div>			
				<div class="col-md-4">
					<div class="form-group">
						<label>Alamat</label>
						<input type="text" name="alamat" placeholder="Alamat" autocomplete="off" class="form-control">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Tempat Lahir</label>
						<input type="text" name="tempat_lahir" placeholder="Tempate Lahir" autocomplete="off" class="form-control">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Tanggak Lahir</label>
						<input type="text" name="tanggal_lahir" id="datepicker" datepicker data-date-format="dd-mm-yyyy" data-date-viewmode="years" class="form-control date-picker">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>No. Hp</label>
						<input type="text" name="phone" placeholder="No. Hp" autocomplete="off" class="form-control">
					</div>
				</div>	
				<div class="col-md-4">
					<div class="form-group">
					<label class="control-label">Jenis Kelamin</label>
						<div>
							<label class="radio-inline">
								<input type="radio" class="grey" value="L" name="jenis_kelamin" checked="" id="gender_female">
								Laki-laki
							</label>
							<label class="radio-inline">
								<input type="radio" class="grey" value="P" name="jenis_kelamin"  id="gender_male">
								Perempuan
							</label>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					<label>Level</label>
						<?php $level = $this->db->query("SELECT * from p_level order by id_level ASC")->result(); ?>
						<select name="level" id="form-field-select-3" class="form-control select" required="">
							<option value="">-- Pilih Level --</option>
							<?php foreach ($level as $lev) { ?>
							<option value="<?php echo $lev->id_level; ?>"><?php echo $lev->level; ?></option>
							<?php }; ?>
						</select>
					</div>
				</div>
			</div>
		</div>	
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-default">Batal</button>
			<button type="submit" class="btn btn-primary">Tambah</button>
		</div>	
	</form>
</div>