<?php 
	$id_record = $this->session->userdata('id_record'); 
	$unit_anda = $this->db->query("SELECT * FROM online_master_penempatan_karyawan where id_record_karyawan = '$id_record'")->result(); 
	$sekarang = date('d F Y');
?>

<div class="row">
	<div class="col-sm-12">
		<div class="page-header">
			<h1>Input Disposisi</h1>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-body">

				<form method="post" enctype="multipart/form-data" action="<?php echo base_url();?>disposisi/test">
					<div class="tujuan">
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label>Pilih Unit</label>
									<select name="dari" id="dari" class="form-control search-select" onchange="changeValue(this.value)">
										<option value=" ">-- Anda Berada Di Unit Mana --</option>
										<?php $jsArray = "var Ddata = new Array();\n";?>
										<?php foreach ($unit_anda as $un) { ?>
											<?php 
												$unit = $this->db->query("SELECT * FROM online_master_unit where id_record = '$un->id_record_unit'")->row();
												$direksi = $this->db->query("SELECT * FROM online_master_direksi where id_record = '$unit->id_record_direksi'")->row();
												$karyawan = $this->db->query("SELECT * FROM online_master_karyawan where id_record = '$un->id_record_karyawan'")->row(); 
											?>
											<option value="<?php echo $un->id_record_unit; ?>"><?php echo $unit->unit; ?></option>

											<?php $jsArray .= 
											"Ddata[	'".$un->id_record_unit ."'] = {
												id_record_unit:		'".$unit->unit ."',
												kepala_unit:		'".$unit->unit ."',

												id_record_direksi:	'".$direksi->id_record ."',
												direksi:			'".$direksi->nama ."',
												nama_lengkap:		'".$direksi->nama_lengkap ."',

												nama:				'".$karyawan->nama ."',
												sekarang:			'".$sekarang ."'
											};\n";
											?>

										<?php }; ?>
									</select>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label>Disposisi</label>
									<select name="kepada" class="form-control search-select">
										<option value="">-- Surat Akan Di Atujukan  Ke Mana --</option>
										<?php $pilih_unit = $this->db->query("SELECT * FROM online_nomor_disposisi where status='1' ORDER BY id_record ASC")->result(); ?>
										<?php foreach ($pilih_unit as $pilih) { ?>
											<option value="<?php echo $pilih->id_record; ?>"><?php echo $pilih->disposisi; ?></option>
										<?php }; ?>
									</select>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label>Judul</label>
									<input type="text" name="judul" class="form-control" autocomplete="off">
								</div>
							</div>
							<div class="col-sm-6">
								<label>
								Filenya
								</label>
								<div class="fileupload fileupload-new" data-provides="fileupload">
									<div class="input-group">
										<div class="form-control uneditable-input">
											<i class="fa fa-file fileupload-exists"></i>
											<span class="fileupload-preview"></span>
										</div>
										<div class="input-group-btn">
											<div class="btn btn-light-grey btn-file">
												<span class="fileupload-new"><i class="fa fa-folder-open-o"></i> Select file</span>
												<span class="fileupload-exists"><i class="fa fa-folder-open-o"></i> Change</span>
												<input type="file" class="file-input">
											</div>
											<a href="#" class="btn btn-light-grey fileupload-exists" data-dismiss="fileupload">
												<i class="fa fa-times"></i> Remove
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="form-surat">
						<h3>BIDANG/DIVISI/INSTALASI/KOMITE/PANITIA/TIM</h3>
						<h3>RSU ISLAM KLATEN</h3>
						<div class="garis_keras"></div>
						<br>
						<p>
							Kepada : <br>
							Direktur Utama <br>
							Ditempat
						</p>
						<br>

						<p>Assalamualaikum wr.wb </p>
						<p>
							Bersama ini kami sampaikan laporan kinerja organisasi 						
							<input type="text" id="id_record_unit" class="form-select" placeholder="...">
							<br>
							Bulan <input type="text" name="bulan" class="form-date" placeholder="..." autocomplete="off" maxlength="2">
							tahun <input type="text" name="tahun" class="form-date" placeholder="..." autocomplete="off" maxlength="4">
							seperti laporan terlampir.
						</p>
						<p>
							Kesimpulan : 
							<input type="text" name="kesimpulan" class="form-select" placeholder="...">
						</p>
						<p>
							Tindakan :
							<input type="text" name="tindakan" class="form-select" placeholder="...">
						</p>

						<p>Demikian untuk menjadikan periksa, atas perhatiannya disampaikan terima kasih.</p>
						<p>Wassalamualaikum wr.wb </p>
						<br>

						<div class="ttd">
							<div class="row">
								<div class="col-sm-6">
									<p>Mengetahui</p>
									<p class="t-ttd">
										<!-- Direktur -->
										<input type="hidden" name="id_record_direksi" id="id_record_direksi" class="form-select" placeholder="..." readonly="">
										<input type="text" name="direksi" id="direksi" class="form-select" placeholder="..." readonly="">
									</p>
									<p class="t-nama"> 
									<input type="text" name="nama_lengkap" id="nama_lengkap" class="form-select" placeholder="..." readonly="">
									</p>
								</div>
								<div class="col-sm-6">
									<p>
										Klaten, <input type="text" id="sekarang" class="form-select" placeholder="..." readonly="">
									</p>
									<p class="t-ttd">
										Kepala 
										<input type="text" id="kepala_unit" class="form-select" placeholder="..." readonly="">
									</p>
									<p class="t-nama"> 
										<input type="text" id="nama" class="form-select" placeholder="..." readonly="">
									</p>
								</div>
							</div>
						</div>
					</div>

					<div class="lampiran-mutu">
						<h3> Lampiran </h3>
						<div class="sarmut">
							<h4>1. Sasaran Mutu</h4>
							<div class="table-responsive">
								<table class="table table-bordered table-hover" id="mydata">
									<thead>
										<tr>
											<th width="5%">No.</th>
											<th width="40">Sasaran Mutu</th>
											<th width="40">Indikator</th>
											<th width="15">Target</th>
										</tr>
									</thead>
									<tbody id="show_data">
									</tbody>
								</table>
							</div>
						</div>

						<div class="program-realisasi">
							<h4>2. Program yang terealisasi</h4>
							<div class="table-responsive">
								<table class="table table-bordered table-hover" id="sample-table-1">
									<thead>
										<tr>
											<th width="5%">No.</th>
											<th width="40%">Sasaran Mutu</th>
											<th width="35%">Indikator</th>
											<th width="10%">Target</th>
											<th width="10%">Pencapaian</th>
										</tr>
									</thead>
									<tbody id="show_data2">
									</tbody>
								</table>
							</div>
						</div>

						<div class="program-fail">
							<h4>3. Program yang tidak tercapai</h4>
							<div class="table-responsive">
								<table class="table table-bordered table-hover" id="sample-table-1">
									<thead>
										<tr>
											<th width="5%">No.</th>
											<th width="30%">Sasaran Mutu</th>
											<th width="25%">Indikator</th>
											<th width="10%">Target</th>
											<th width="10%">Pencapaian</th>
											<th width="20%">Keterangan</th>
										</tr>
									</thead>
									<tbody id="show_data3">
									</tbody>
								</table>
							</div>
						</div>

						<div class="diluar-program">
							<h4>4. Kegiatan diluar program</h4>
							<div class="table-responsive">
								<table class="table table-bordered table-hover" id="sample-table-1">
									<thead>
										<tr>
											<th width="5%">No.</th>
											<th>Program</th>
											<th>Urgensi</th>
											<th>Pencapaian</th>
										</tr>
									</thead>
									<tbody id="show_data4">
									</tbody>
								</table>
							</div>
						</div>					
					</div>

					<div class="row">
						<div class="pull-right" style="margin-right: 20px;">
							<input type="hidden" value="<?php echo $id_record;?>" name="id_record_karyawan" class="form-control">
							<button type="submit" class="btn btn-primary start"> <span>Simpan</span> </button>
						</div>
					</div>
				</form>
			</div>

		</div>
	</div>
</div>

<script type="text/javascript">    
	<?php echo $jsArray; ?>  
	function changeValue(x){  
		document.getElementById('id_record_unit').value = Ddata[x].id_record_unit;  
		document.getElementById('kepala_unit').value = Ddata[x].kepala_unit;    
		document.getElementById('id_record_direksi').value = Ddata[x].id_record_direksi;    
		document.getElementById('direksi').value = Ddata[x].direksi;  
		document.getElementById('nama').value = Ddata[x].nama;    
		document.getElementById('nama_lengkap').value = Ddata[x].nama_lengkap;    
		document.getElementById('sekarang').value = Ddata[x].sekarang;

		$.ajax({
			url: "<?php echo base_url('disposisi/cek'); ?>",
			type : "post",
			data:{id:x},
			success: function(data){
				console.log(data)
				var html = '';
				var i;
				no=1;
				for(i=0; i<data.length; i++){
					html += '<tr>'+
					'<td>'+no+'</td>'+
					'<td>'+data[i].sarmut+'</td>'+
					'<td>'+data[i].indikator+'</td>'+
					'<td>'+data[i].target+'</td>'+
					'</tr>';
					no++
				}
				$('#show_data').html(html);
			}
		})

		$.ajax({
			url: "<?php echo base_url('disposisi/cek2'); ?>",
			type : "post",
			data:{id:x},
			success: function(data){
				console.log(data)
				var html2 = '';
				var i;
				no=1;
				for(i=0; i<data.length; i++){
					html2 += '<tr>'+
					'<td>'+no+'</td>'+
					'<td>'+data[i].sarmut+'</td>'+
					'<td>'+data[i].indikator+'</td>'+
					'<td>'+data[i].target+'</td>'+
					'<td>'+data[i].pencapaian+'</td>'+
					'</tr>';
					no++
				}
				$('#show_data2').html(html2);
			}
		})

		$.ajax({
			url: "<?php echo base_url('disposisi/cek3'); ?>",
			type : "post",
			data:{id:x},
			success: function(data){
				console.log(data)
				var html3 = '';
				var i;
				no=1;
				for(i=0; i<data.length; i++){
					html3 += '<tr>'+
					'<td>'+no+'</td>'+
					'<td>'+data[i].sarmut+'</td>'+
					'<td>'+data[i].indikator+'</td>'+
					'<td>'+data[i].target+'</td>'+
					'<td>'+data[i].pencapaian+'</td>'+
					'<td>'+data[i].keterangan+'</td>'+
					'</tr>';
					no++
				}
				$('#show_data3').html(html3);
			}
		})

		$.ajax({
			url: "<?php echo base_url('disposisi/cek4'); ?>",
			type : "post",
			data:{id:x},
			success: function(data){
				console.log(data)
				var html4 = '';
				var i;
				no=1;
				for(i=0; i<data.length; i++){
					html4 += '<tr>'+
					'<td>'+no+'</td>'+
					'<td>'+data[i].program+'</td>'+
					'<td>'+data[i].urgensi+'</td>'+
					'<td>'+data[i].pencapaian+'</td>'+
					'</tr>';
					no++
				}
				$('#show_data4').html(html4);
			}
		})
	};  
</script>