<div class="row">
	<div class="col-sm-12">
		<div class="page-header">
			<h1><?php echo $title;?></h1>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-3 pull-right">
		<form method="POST" action="<?php echo site_url('pesan/search');?>">
			<div class="input-group">
				<input type="text" name="search" onkeyup="pencarian_tabel()" class="form-control" placeholder="Search..." autocomplete="off">
				<span class="input-group-btn">
					<button class="btn btn-primary" type="submit"> <i class="fa fa-search"></i> </button>
				</span>
			</div>
		</form>
	</div>
</div>

<br>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<i class="fa fa-external-link-square"></i>
				<?php echo $description;?>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover" id="sample-table-2">
						<thead>
							<tr>
								<th class="center">NO</th>
								<th>Pengirim</th>
								<th>Unit Pengirim</th>
								<th>Kepada Unit</th>
								<th>Judul</th>
								<th>Tanggal</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php if (empty($pesan)) { ?>
							<tr><td colspan="12" style="text-align: center; font-weight: bold">-- Tidak ada Kotak Masuk --</td></tr>
							<?php } else { ?>
							<?php $nomor=0; foreach ($pesan as $row): ?>
							<tr>
								<td class="text-center">
									<?php echo $nomor=$nomor+1; ?>
								</td>
								<td>
									<a href="<?php echo base_url();?>pesan/pdf/<?php echo $row->id_record_surat_disposisi;?>" rel="nofollow">
										<?php $dunit = $this->db->query("SELECT * FROM online_master_karyawan where id_record = '$row->id_record_karyawan_from'")->row_array();
										echo $dunit['nama']; ?>
									</a>
									<?php if($row->status_kirim_oleh_sekret=='f'){ ?>
										<span class="badge badge-new"> new </span>
									<?php }else{ ?>

									<?php } ?>
								</td>
								<td>
									<?php
									$dunit = $this->db->query("SELECT * FROM online_master_unit where id_record = '$row->id_record_unit_from'")->row_array();
									echo $dunit['unit'];
									?>
								</td>
								<td>
									<?php
									$dunit = $this->db->query("SELECT * FROM online_master_unit where id_record = '$row->id_record_unit_to'")->row_array();
									echo $dunit['unit'];
									?>
								</td>
								<td>
									<?php
									$osd = $this->db->query("SELECT * FROM online_surat_disposisi where id_record = '$row->id_record_surat_disposisi'")->row_array();
									echo $osd['judul'];
									?>
								</td>
								<td><?php echo $row->date_kirim_oleh_sekret;?></td>
								<!-- <td>
									<?php if($row->ket==1){ ?>
										<span class="label label-warning"> Proses</span>
									<?php }elseif($row->ket==2){ ?>
										<span class="label label-success"> Selesai</span>
									<?php }else{ ?>
										<span class="label label-danger"> Batal</span>
									<?php } ?>
								</td> -->
								<td class="center">
									<!-- <a href="<?php echo base_url();?>pesan/lihat/<?php echo $row->id_record;?>" class="btn btn-xs btn-warning tooltips" data-original-title="Edit">Lihat</a> -->
									<a href="<?php echo base_url();?>pesan/balas/<?php echo $row->id_record;?>" class="btn btn-xs btn-teal tooltips" data-original-title="Balas"><i class="fa fa-mail-reply"></i></a>
								</td>
							</tr>
							<?php endforeach; ?>
							<?php }; ?>
						</tbody>
					</table>
					<div class="text-center mt-3">
						<?php echo $this->pagination->create_links(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>