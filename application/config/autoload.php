<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages'] = array();

$autoload['libraries'] = array('session', 'database','pagination');

$autoload['drivers'] = array();

$autoload['helper'] = array('url', 'form', 'file', 'text', 'security');

$autoload['config'] = array();

$autoload['language'] = array();

$autoload['model'] = array('main_model','security_model');
