<?php
class Main_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function upload_file($filename)
    {
        $config['upload_path'] = './excel/';
        $config['allowed_types'] = 'xlsx';
        $config['max_size'] = '5048';
        $config['overwrite'] = true;
        $config['file_name'] = $filename;
    
        $this->upload->initialize($config);
        if($this->upload->do_upload('file'))
        {
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
            return $return;
        }
        else
        {
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
            return $return;
        }
    }


    function cek_allready($table, $data)
    {
        $this->db->get_where($table, $data);
        return $this->db->affected_rows() > 0 ? TRUE : FALSE;
    }
    
    function Get_all($table)
    {
        return $this->db->get($table)->result();
    }

    function Get_limit($number,$table)
    {
        return $this->db->get($table,$number)->result();
    }

    function Get_limit_offset($number,$offset,$table)
    {
        return $this->db->get($table,$number,$offset)->result();
    }

    public function pagination_where($table, $per_page, $offset, $where, $order_by)
    {
        $this->db->select('*');
        $this->db->where($where);
        $this->db->order_by($order_by);
        return $this->db->get($table, $per_page, $offset)->result();
    }
    
    function Pagination_all($number,$offset,$table)
    {
        return $this->db->get($table,$number,$offset)->result();
    }
    
    function Pagination($number,$offset,$table)
    {
        // $this->db->order_by('id_record desc');
        return $this->db->get($table,$number,$offset)->result();
    }

    function provinsi()
    {
        $this->db->order_by('name','ASC');
        $provinces= $this->db->get('provinces');
        return $provinces->result_array();
    }

    function kabupaten($provId)
    {
        $kabupaten="<option value='0'>--pilih--</pilih>";
        $this->db->order_by('name','ASC');
        $kab= $this->db->get_where('regencies',array('province_id'=>$provId));
        foreach ($kab->result_array() as $data )
        {
            $kabupaten.= "<option value='$data[id]'>$data[name]</option>";
        }
        return $kabupaten;
    }

    function kecamatan($kabId)
    {
        $kecamatan="<option value='0'>--pilih--</pilih>";
        $this->db->order_by('name','ASC');
        $kec= $this->db->get_where('districts',array('regency_id'=>$kabId));
        foreach ($kec->result_array() as $data )
        {
            $kecamatan.= "<option value='$data[id]'>$data[name]</option>";
        }
        return $kecamatan;
    }
    
    function Insert_something($table,$data)
    {
        $action = $this->db->insert($table, $data);  
        return $action;
    }


    public function Update_something($table, $data, $column, $key)
    {
        $this->db->set($data);
        $this->db->where($column, $key);
        return $this->db->update($table);
    }

    function Update_double($table,$data,$wher1,$wher2)
    {
        $this->db->set($data);
        $this->db->where($wher1);
        $this->db->where($wher2);
        $action=$this->db->update($table);
        return $action;
    }

    function Search($keyword,$column,$table)
    {
        $this->db->like($column,$keyword);
        $query=$this->db->get($table);
        return $query->result();
    }

    function Count_where($table,$data)
    {
        $this->db->where($data);
        $this->db->from($table);
        return $this->db->count_all_results();
    }

    function Count_total($table)
    {
        $this->db->from($table);
        return $this->db->count_all_results();
    }
    
    function Getsomething1($table,$data)
    {   
        $this->db->select('*');
        $this->db->where('id_user',$data);
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get($table);
        return $query->result();
    }

    function Getsomething($table,$data)
    {
        return $this->db->get_where($table,$data)->result();
    }

    public function Delete_something($table, $data)
    {
        $this->db->delete($table, $data);
    }

    function Search_pesan($keyword)
    {
        $this->db->like('id_record',$keyword);
        $this->db->or_like('id_record',$keyword);
        $this->db->or_like('judul',$keyword);
        $this->db->order_by('id_record',"DESC");
        $query=$this->db->get('d_kirim_disposisi');
        return $query->result();
    }

    function Search_unit($keyword)
    {
        $this->db->like('id_record',$keyword);
        $this->db->or_like('unit',$keyword);
        $query=$this->db->get('online_master_unit');
        return $query->result();
    }

    function Search_karyawan($keyword)
    {
        $this->db->like('nip',$keyword);
        $this->db->or_like('nama',$keyword);
        $query=$this->db->get('online_master_karyawan');
        return $query->result();
    }

    function Auto_id($column,$part,$table)
    {
        $query = $this->db->query("select MAX(RIGHT($column,3)) as sta from $table");
        $id = "";
        if($query->num_rows()>0)
        {
            foreach($query->result() as $cd)
            {
                $tmp = ((int)$cd->sta)+1;
                $id = sprintf("%03s", $tmp);
            }
        }
        else
        {
            $id = "001";
        }
        return $part.$id;
    }
}