<?php class Security_Model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function loggedin_check()
	{
		if ($this->session->userdata('logged_in') == FALSE)
		{
			$this->session->set_flashdata("message", "Dilarang mengakses, Silahkan Login Terlebih Dahulu");
			redirect('login', 'refresh');
		}
	}

	function loggedin_check_admin()
	{
		$id_admin = $this->session->userdata('id_admin');
		$query = $this->db->query("SELECT * FROM p_administrator WHERE id_admin='$id_admin'")->row_array();

		if (($this->session->userdata('logged_in') == FALSE) or (empty($query['id_admin'])))
		{
			$this->session->set_flashdata("message", "Dilarang mengakses, Silahkan Login Terlebih Dahulu");
			redirect('admin/login', 'refresh');
		}
	}

}
?>
				