<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function index()
    {
        $data['title']       = 'Login';
        $data['description'] = 'Halaman Login';
        $data['keywords']    = 'Login';
        $data['page']        = 'login';
        $this->load->view('temp_login', $data);

        if(isset($_POST['login']))
        {
            $nip = $this->input->post('nip');
            $password =$this->input->post('password');

            $data_login = shell_exec("E://".'xampp/htdocs/disposisi/application/app/login --nik '.$nip.' --password '.$password);
            // echo "<br>".$data_login;
            $hasil = json_decode($data_login)->response;

            if($hasil == 'true'){
                 $query = $this->db->query("SELECT * FROM online_master_karyawan WHERE nip='$nip'")->row_array();
                 $unit = $query['id_record'];
                    $unit = $this->db->query("SELECT * FROM online_master_penempatan_karyawan WHERE id_record_karyawan='$unit'")->row_array();

                 $session = array(
                        'id_record' => $query['id_record'],
                        'nip'       => $query['nip'],
                        'nama'      => $query['nama'],
                        'id_unit'   => $unit['id_record_unit'],
                        'logged_in' => TRUE
                        );
                    $this->session->set_userdata($session);

                    $this->session->set_flashdata('toast-success', 'Berhasil Login.');
                    redirect('dashboard', 'refresh');
            }else{
                $status = [
                'response'=>'bad'
                ];
                $this->session->set_flashdata('callout-danger', 'Periksa kembali NIK dan Password anda.');
                redirect('login');
            }
        }
    }

    // public function index()
    // {
    //     $data['title']       = 'Login';
    //     $data['description'] = 'Halaman Login';
    //     $data['keywords']    = 'Login';
    //     $data['page']        = 'login';
    //     $this->load->view('temp_login', $data);

    //     if(isset($_POST['login']))
    //     {
    //         $nip = $this->input->post('nip');
    //         $password = md5($this->input->post('password'));

    //         $query = $this->db->query("SELECT * FROM online_master_karyawan WHERE nip='$nip'")->row_array();

    //         if(empty($query))
    //         {
    //             $this->session->set_flashdata('callout-danger', 'Karyawan tidak ditemukan.');
    //             redirect('login');
    //         }
    //         else
    //         {
    //             if($password == $query['password'])
    //             {
    //                 $unit = $query['id_record'];
    //                 $unit = $this->db->query("SELECT * FROM online_master_penempatan_karyawan WHERE id_record_karyawan='$unit'")->row_array();

    //                 $session = array(
    //                     'id_record' => $query['id_record'],
    //                     'nip'       => $query['nip'],
    //                     'nama'      => $query['nama'],
    //                     'id_unit'   => $unit['id_record_unit'],
    //                     'logged_in' => TRUE
    //                     );
    //                 $this->session->set_userdata($session);

    //                 $this->session->set_flashdata('toast-success', 'Berhasil Login.');
    //                 redirect('dashboard', 'refresh');
    //             }
    //             else
    //             {
    //                 $this->session->set_flashdata('callout-danger', 'Password yang anda masukan salah.');
    //                 redirect('login');
    //             }
    //         }
    //     }
    // }

    public function logout()
    {
        $this->session->unset_userdata('id_record');
        $this->session->unset_userdata('nip');
        $this->session->unset_userdata('nama');
        $this->session->unset_userdata('id_unit');
        $this->session->unset_userdata('logged_in');
        $this->session->sess_destroy();

        $this->session->set_flashdata('callout-success', 'Berhasil Logout.');
        redirect('login');
    }
}
