<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->security_model->loggedin_check();
	}

	public function Pilih_unit()
	{
		$unit_terpilih = $this->input->post('id_record_unit');
		$url = $this->input->post('url');

		if($url=='sarmut'){
			$session = array('id_unit' => $unit_terpilih);
			$this->session->set_userdata($session);
			redirect('master/sarmut/'.$unit_terpilih);
		}elseif($url=='program_unit'){
			$session = array('id_unit' => $unit_terpilih);
			$this->session->set_userdata($session);
			redirect('master/program_unit/'.$unit_terpilih);
		}else{			
			$session = array('id_unit' => $unit_terpilih);
			$this->session->set_userdata($session);
			redirect('master/program_diluar_unit/'.$unit_terpilih);
		}
	}

	public function Sarmut($id_unit)
	{
		$data['title']       = 'Data Master Sasaran Mutu';
        $data['description'] = 'Data Master Sasaran Mutu';
        $data['keywords']    = '';
        
        $data['unit'] = $this->db->query("SELECT * FROM online_master_unit where id_record='$id_unit'")->row();
        $local = $this->load->database('localhost', TRUE);
        $data['data'] = $local->query("SELECT * FROM master_sasaran_mutu WHERE id_record_unit='$id_unit'")->result();

        $data['page'] = 'sasaran_mutu';
        $this->load->view('index', $data);
    }

    public function Program_unit($id_unit)
	{
		$data['title']       = 'Data Master Program Unit';
        $data['description'] = 'Data Master Program Unit';
        $data['keywords']    = '';
        
        $data['unit'] = $this->db->query("SELECT * FROM online_master_unit where id_record='$id_unit'")->row();
        $local = $this->load->database('localhost', TRUE);
        $data['data'] = $local->query("SELECT * FROM master_program_unit WHERE id_record_unit='$id_unit'")->result();

        $data['page'] = 'program_unit';
        $this->load->view('index', $data);
    }

    public function Program_diluar_unit($id_unit)
	{
		$data['title']       = 'Data Master Kegiatan Diluar Program';
        $data['description'] = 'Data Master Kegiatan Diluar Program';
        $data['keywords']    = '';
        
        $data['unit'] = $this->db->query("SELECT * FROM online_master_unit where id_record='$id_unit'")->row();
        $local = $this->load->database('localhost', TRUE);
        $data['data'] = $local->query("SELECT * FROM master_diluar_program_unit WHERE id_record_unit='$id_unit'")->result();

        $data['page'] = 'program_diluar_unit';
        $this->load->view('index', $data);
    }

    public function Tambah_data_sarmut()
	{
		$id_unit 		= $this->input->post('id_unit');
		$sasaran_mutu 	= $this->input->post('sasaran_mutu');
		$indikator 		= $this->input->post('indikator');
		$target 		= $this->input->post('target');

		$local = $this->load->database('localhost', TRUE);
		$query = $local->query("
			INSERT INTO master_sasaran_mutu (id_record_unit, sarmut, indikator, target, status)
			VALUES ('$id_unit', '$sasaran_mutu', '$indikator', '$target', '1')");

		if ($query)
		{
			$this->session->set_flashdata('toast-success', 'Data Berhasil Tersimpan');
			redirect('master/sarmut/'.$id_unit);
		}
		else
		{
			$this->session->set_flashdata('toast-error', 'Data Gagal Tersimpan');
			redirect('master/sarmut/'.$id_unit);
		}
	}

	public function Tambah_data_program()
	{
		$id_unit = $this->input->post('id_unit');
		$sasaran_mutu 	= $this->input->post('sasaran_mutu');
		$indikator 		= $this->input->post('indikator');
		$target 		= $this->input->post('target');
		$pencapaian 	= $this->input->post('pencapaian');
		$keterangan 	= $this->input->post('keterangan');
		$status_program = $this->input->post('status_program');

		$local = $this->load->database('localhost', TRUE);
		$query = $local->query("
			INSERT INTO master_program_unit (id_record_unit, sarmut, indikator, target, pencapaian, keterangan, status_program, status)
			VALUES ('$id_unit', '$sasaran_mutu', '$indikator', '$target', '$pencapaian','$keterangan','$status_program', '1')");

		if ($query)
		{
			$this->session->set_flashdata('toast-success', 'Data Berhasil Tersimpan');
			redirect('master/program_unit/'.$id_unit);
		}
		else
		{
			$this->session->set_flashdata('toast-error', 'Data Gagal Tersimpan');
			redirect('master/program_unit/'.$id_unit);
		}
	}

	public function Tambah_program_diluar_unit()
	{
		$id_unit = $this->input->post('id_record_unit');

		$program 	= $this->input->post('program');
		$urgensi 	= $this->input->post('urgensi');
		$pencapaian = $this->input->post('pencapaian');

		$local = $this->load->database('localhost', TRUE);
		$query = $local->query("
			INSERT INTO master_diluar_program_unit (id_record_unit, program, urgensi, pencapaian)
			VALUES ('$id_unit', '$program', '$urgensi', '$pencapaian')");

		if ($query)
		{
			$this->session->set_flashdata('toast-success', 'Data Berhasil Tersimpan');
			redirect('master/program_diluar_unit/'.$id_unit);
		}
		else
		{
			$this->session->set_flashdata('toast-error', 'Data Gagal Tersimpan');
			redirect('master/program_diluar_unit/'.$id_unit);
		}
	}

	public function Edit_data_sarmut()
	{
		$id = $this->input->post('id');
		$id_unit = $this->input->post('id_record_unit');

		$sasaran_mutu 	= $this->input->post('sasaran_mutu');
		$indikator 		= $this->input->post('indikator');
		$target 		= $this->input->post('target');
		$local = $this->load->database('localhost', TRUE);
		$query = $local->query("UPDATE master_sasaran_mutu SET 
			sarmut='$sasaran_mutu',
			indikator='$indikator',
			target='$target'
			where id='$id'");

		if ($query)
		{
			$this->session->set_flashdata('toast-success', 'Data Berhasil Disimpan');
			redirect('master/sarmut/'.$id_unit);
		}
		else
		{
			$this->session->set_flashdata('toast-error', 'Data Gagal Disimpan');
			redirect('master/sarmut/'.$id_unit);
		}
	}

	public function Edit_data_program()
	{
		$id = $this->input->post('id');
		$id_unit = $this->input->post('id_record_unit');

		$sasaran_mutu 	= $this->input->post('sasaran_mutu');
		$indikator 		= $this->input->post('indikator');
		$target 		= $this->input->post('target');
		$pencapaian 	= $this->input->post('pencapaian');
		$keterangan 	= $this->input->post('keterangan');
		$status_program = $this->input->post('status_program');
		$local = $this->load->database('localhost', TRUE);
		$query = $local->query("UPDATE master_program_unit SET 
			sarmut='$sasaran_mutu',
			indikator='$indikator',
			target='$target',
			pencapaian='$pencapaian',
			keterangan='$keterangan',
			status_program='$status_program'
			where id='$id'");

		if ($query)
		{
			$this->session->set_flashdata('toast-success', 'Data Berhasil Disimpan');
			redirect('master/program_unit/'.$id_unit);
		}
		else
		{
			$this->session->set_flashdata('toast-error', 'Data Gagal Disimpan');
			redirect('master/program_unit/'.$id_unit);
		}
	}

	public function Edit_program_diluar_unit()
	{
		$id = $this->input->post('id');
		$id_unit = $this->input->post('id_record_unit');

		$program 	= $this->input->post('program');
		$urgensi 	= $this->input->post('urgensi');
		$pencapaian = $this->input->post('pencapaian');

		$local = $this->load->database('localhost', TRUE);
		$query = $local->query("UPDATE master_diluar_program_unit SET 
			program='$program',
			urgensi='$urgensi',
			pencapaian='$pencapaian'
			where id='$id'");

		if ($query)
		{
			$this->session->set_flashdata('toast-success', 'Data Berhasil Disimpan');
			redirect('master/program_diluar_unit/'.$id_unit);
		}
		else
		{
			$this->session->set_flashdata('toast-error', 'Data Gagal Disimpan');
			redirect('master/program_diluar_unit/'.$id_unit);
		}
	}

	public function Hapus($id,$id_unit)
	{
        $local = $this->load->database('localhost', TRUE);
		$query = $local->query("DELETE FROM master_sasaran_mutu WHERE id = '$id'");
        
        $this->session->set_flashdata('toast-success', 'Berhasil Menghapus data');
        redirect('master/sarmut/'.$id_unit);
	}

	public function Hapus_program($id,$id_unit)
	{
		$local = $this->load->database('localhost', TRUE);
		$query = $local->query("DELETE FROM master_program_unit WHERE id = '$id'");
        
        $this->session->set_flashdata('toast-success', 'Berhasil Menghapus data');
        redirect('master/program_unit/'.$id_unit);
	}

	public function Hapus_program_diluar_unit($id,$id_unit)
	{
		$local = $this->load->database('localhost', TRUE);
		$query = $local->query("DELETE FROM master_diluar_program_unit WHERE id = '$id'");
        
        $this->session->set_flashdata('toast-success', 'Berhasil Menghapus data');
        redirect('master/program_diluar_unit/'.$id_unit);
	}
}