<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->security_model->loggedin_check();
	}
	
	public function index()
	{
		$data['title']       = 'Daftar Disposisi';
        $data['description'] = 'Daftar Disposisi';
        $data['keywords']    = '';
        $data['page']        = 'dashboard';
        $this->load->view('index', $data);
    }
}