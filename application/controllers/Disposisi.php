<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Disposisi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->security_model->loggedin_check();
	}
	
	public function index()
	{		
		$data['title']       = 'Data Disposisi';
        $data['description'] = 'Halaman Data Disposisi';
        $data['keywords']    = 'Data Disposisi';
        $data['page']        = 'disposisi_input';
        $this->load->view('index', $data);
	}

	function cek()
	{
		$id_unit  = $this->input->post('id', true);

		$local = $this->load->database('localhost', TRUE);
		$sarmut = $local->query("SELECT * FROM master_sasaran_mutu WHERE id_record_unit='$id_unit'")->result(); 
		$this->output->set_content_type('application/json')->set_output(json_encode($sarmut));
	}

	function cek2()
	{
		$id_unit  = $this->input->post('id', true);

		$local = $this->load->database('localhost', TRUE);
		$program_unit = $local->query("SELECT * FROM master_program_unit WHERE id_record_unit='$id_unit'and status_program= 'Y'")->result(); 
		$this->output->set_content_type('application/json')->set_output(json_encode($program_unit));
	}

	function cek3()
	{
		$id_unit  = $this->input->post('id', true);

		$local = $this->load->database('localhost', TRUE);
		$program_unit2 = $local->query("SELECT * FROM master_program_unit WHERE id_record_unit='$id_unit'and status_program= 'T'")->result(); 
		$this->output->set_content_type('application/json')->set_output(json_encode($program_unit2));
	}

	function cek4()
	{
		$id_unit  = $this->input->post('id', true);

		$local = $this->load->database('localhost', TRUE);
		$program_diluar = $local->query("SELECT * FROM master_diluar_program_unit WHERE id_record_unit='$id_unit'")->result(); 
		$this->output->set_content_type('application/json')->set_output(json_encode($program_diluar));
	}

	public function Test()
	{
		$id_record_karyawan = $this->input->post('id_record_karyawan');

		$unit_pengirim 	= $this->input->post('dari');
		$unit_penerima 	= $this->input->post('kepada');
		$judul 			= $this->input->post('judul');
		$file			= '';

		$pjg = strlen($this->input->post('bulan'));
		if($pjg>1){
			$bulan = $this->input->post('bulan');
		}else{
			$bulan = '0'.$this->input->post('bulan');
		}
		$tahun = $this->input->post('tahun');
		$kesimpulan = $this->input->post('kesimpulan');
		$tindakan 	= $this->input->post('tindakan');


		echo $direksi 	= $this->input->post('direksi');
		echo $direksi 	= $this->input->post('nama_lengkap');



		// $dat = $this->db->query("SELECT * FROM online_master_karyawan where id_record='$id_record_karyawan'")->row();
		// echo $dat->nama;
	}

	public function Kirim()
	{
		$id_record_karyawan 	= $this->input->post('id_record_karyawan');		
		$kepada = $this->input->post('kepada');

		// $this->db->set('id_record','UUID()',FALSE);
		$this->load->library('uuid');
		$uuid = $this->uuid->v4(); 

		$data = array(
			'id_record'					=> $uuid,
			'id_record_unit_pengirim'	=> $this->input->post('dari'),
			'id_record_karyawan'		=> $id_record_karyawan,
			// 'id_unit_penerima'		=> $this->input->post('kepada'),
			'judul'	 					=> $this->input->post('judul'),
			'status_final'	 			=> 'f',
			'date_create'	 			=> date_create('now', timezone_open('Asia/Jakarta'))->format('Y-m-d H:i:s')
			);		
		$query = $this->db->insert('online_surat_disposisi', $data);

		//Direktur utama
		if($kepada == '7f06baf2-999a-11ea-8e7c-c2a331db3dd4'){
			$id_record_unit_to = 'b28b682c-a56e-11ea-9631-c2a331db3dd4';

        //Direktur Pelayanan Medis
		}elseif($kepada == '7f06bdbb-999a-11ea-8e7c-c2a331db3dd4'){
			$id_record_unit_to = 'b28b5147-a56e-11ea-9631-c2a331db3dd4';

		//Direktur Penunjang Medis
		}elseif($kepada == '7f06be46-999a-11ea-8e7c-c2a331db3dd4'){
			$id_record_unit_to = 'b28b575a-a56e-11ea-9631-c2a331db3dd4';

		//Direktur adm umum
		}elseif($kepada == '7f06be6e-999a-11ea-8e7c-c2a331db3dd4'){
			$id_record_unit_to = 'b28b580b-a56e-11ea-9631-c2a331db3dd4';
		}

		$unit = $this->db->query("SELECT * FROM online_master_penempatan_karyawan WHERE id_record_unit='$id_record_unit_to'")->row_array();
		$id_record_karyawan_to = $unit['id_record_karyawan'];

		$data2 = array(
			'id_record'					=> $this->uuid->v4(),
			'id_record_surat_disposisi'	=> $uuid,
			'id_record_karyawan_from'	=> $id_record_karyawan,
			'id_record_unit_from'		=> $this->input->post('dari'),
			'id_record_karyawan_to'		=> $id_record_karyawan_to,
			'id_record_unit_to'			=> $id_record_unit_to,
			'status_balas'	 			=> 'f',
			'status_kirim_oleh_sekret'	=> 'f'
			// 'date_kirim_oleh_sekret'	=> date_create('now', timezone_open('Asia/Jakarta'))->format('Y-m-d H:i:s')
			);		
		$query = $this->db->insert('online_proses_kirim_disposisi', $data2);

		if ($query)
		{
			$this->session->set_flashdata('toast-success', 'Disposisi Berhasil Terkirim');
			redirect('disposisi');
		}
		else
		{
			$this->session->set_flashdata('toast-error', 'Disposisi Gagal Terkirim');
			redirect('disposisi');
		}
	}
}