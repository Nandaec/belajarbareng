<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->security_model->loggedin_check_admin();
	}

	public function index()
	{
		$data['title']       = 'Data Administrator';
        $data['description'] = 'Data Administrator';
        $data['keywords']    = '';

        $tahun = date('Y');
        $id_admin = '21'.$tahun;
        $data['id_admin'] = $this->main_model->Auto_id('id_admin',$id_admin,'p_administrator');

        $data['administrator']  = $this->db->query("SELECT * FROM p_administrator")->result();
        $data['page']           = 'backoffice/administrator';
        $this->load->view('backoffice/index', $data);
    }

    public function Tambah()
    {
        $data = array(
            'id_admin'      => $this->input->post('id_admin'),
            'nama'          => $this->input->post('nama'),
            'email'         => $this->input->post('email'),
            'username'      => $this->input->post('username'),
            'password'      => md5('123456'),
            'jenis_kelamin' => $this->input->post('jenis_kelamin'),
            'phone'         => $this->input->post('phone'),
            'alamat'        => $this->input->post('alamat'),
            'tempat_lahir'  => $this->input->post('tempat_lahir'),
            'tanggal_lahir' => $this->input->post('tanggal_lahir'),
            'level'         => $this->input->post('level'),
            'tanggal_register'=> date_create('now', timezone_open('Asia/Jakarta'))->format('Y-m-d H:i:s')
            );      
        $query = $this->db->insert('p_administrator', $data);

        if ($query)
        {
            $this->session->set_flashdata('toast-success', 'Data Berhasil Terkirim');
            redirect('backoffice/administrator');
        }
        else
        {
            $this->session->set_flashdata('toast-error', 'Data Gagal Terkirim');
            redirect('backoffice/administrator');
        }
    }

    public function Edit()
    {
        $id_admin = $this->input->post('id_admin');
        $data = array(
            'nama'          => $this->input->post('nama'),
            'email'         => $this->input->post('email'),
            'username'      => $this->input->post('username'),
            'password'      => md5('123456'),
            'jenis_kelamin' => $this->input->post('jenis_kelamin'),
            'phone'         => $this->input->post('phone'),
            'alamat'        => $this->input->post('alamat'),
            'tempat_lahir'  => $this->input->post('tempat_lahir'),
            'tanggal_lahir' => $this->input->post('tanggal_lahir'),
            'level'         => $this->input->post('level'),
            'tanggal_register'=> date_create('now', timezone_open('Asia/Jakarta'))->format('Y-m-d H:i:s')
            );      
        $query = $this->main_model->update_something('p_administrator', $data, 'id_admin', $id_admin);

        if ($query)
        {
            $this->session->set_flashdata('toast-success', 'Data Berhasil Terkirim');
            redirect('backoffice/administrator');
        }
        else
        {
            $this->session->set_flashdata('toast-error', 'Data Gagal Terkirim');
            redirect('backoffice/administrator');
        }
    }

    public function Hapus($id_admin=0)
    {
        $data=array('id_admin' => $id_admin);
        $query = $this->main_model->delete_data('p_administrator',$data);
        if ($query)
        {
            $this->session->set_flashdata('toast-success', 'Berhasil menghapus data');
            redirect('backoffice/administrator');
        }
        else
        {
            $this->session->set_flashdata('toast-error', 'Gagal menghapus data');
            redirect('backoffice/administrator');
        }
    }






    public function Tambah_karyawan()
    {
      $this->load->library('uuid');
      $uuid = $this->uuid->v4();

      $data = array(
         'id_record'	=> $uuid,
         'nip'		=> $this->input->post('nip2'),
         'password'	=> md5('123456'),
         'nama'		=> $this->input->post('nama'),
         'unit'	 	=> $this->input->post('unit'),
         'jabatan'	=> $this->input->post('jabatan')
         );		
      $query = $this->main_model->insert_something('online_master_karyawan', $data);

      if ($query)
      {
         $this->session->set_flashdata('toast-success', 'Data Berhasil Tersimpan');
         redirect('backoffice/karyawan');
     }
     else
     {
         $this->session->set_flashdata('toast-error', 'Data Gagal Tersimpan');
         redirect('backoffice/karyawan');
     }
 }

 public function Edit_karyawan()
 {
  $id = $this->input->post('unit_sekretariat');
		// $jum=$this->db->query("SELECT * FROM online_master_unit WHERE id_record = '$id'")->row_array();
		// echo $jum['unit'];

  $del=array('id_record_karyawan' => $this->input->post('id_record_karyawan'));
  $this->main_model->delete_something('online_master_penempatan_karyawan', $del);
  $sekre = '';
  foreach($this->input->post('unit_sekretariat') as $sekre) {
     $this->load->library('uuid');
     $uuid = $this->uuid->v4();
     $data_legal = array(
        'id_record' 		=> $uuid,
        'id_record_karyawan'=> $this->input->post('id_record_karyawan'),
        'id_record_unit' 	=> $sekre,
        'create_date'		=> date_create('now', timezone_open('Asia/Jakarta'))->format('Y-m-d H:i:s')
        );

     $query = $this->db->insert('online_master_penempatan_karyawan', $data_legal);
 }

 if ($query)
 {
     $this->session->set_flashdata('toast-success', 'Data Berhasil Tersimpan');
     redirect('backoffice/karyawan');
 }
 else
 {
     $this->session->set_flashdata('toast-error', 'Data Gagal Tersimpan');
     redirect('backoffice/karyawan');
 }
}

public function Search()
{
    $input = $this->input->post('search');
    $jum = $this->db->query("SELECT * FROM online_master_karyawan WHERE nip LIKE '%$input%'")->num_rows();

    $data['title']       = 'Pencarian Data Karyawan';
    $data['description'] = 'Jumlah Pencarian Data Karyawan <strong>'.$input.'</strong> berjumlah <strong>'.$jum.'</strong>';
    $data['keywords']    = '';
    $key=$this->input->post('search');
    $data['num']=0;
    $data['karyawan']=$this->main_model->search_karyawan($key);
    $data['page']        = 'backoffice/karyawan';
    $this->load->view('backoffice/index', $data);
}

 //    public function Hapus($id_record)
	// {
	// 	$hpus1=array('id_record_karyawan' => $id_record);
	// 	$hpus2=array('id_record' => $id_record);
 //        $this->main_model->delete_something('online_master_penempatan_karyawan',$hpus1);
 //        $this->main_model->delete_something('online_master_karyawan',$hpus2);
 //        $this->session->set_flashdata('toast-success', 'Berhasil Menghapus data');
 //        redirect("backoffice/karyawan");
	// }
}