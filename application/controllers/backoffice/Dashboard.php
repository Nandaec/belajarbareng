<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->security_model->loggedin_check_admin();
	}

	public function index()
	{
		$data['title']       = 'Dashboard';
        $data['description'] = 'Dashboard';
        $data['keywords']    = '';
        $data['page']        = 'backoffice/dashboard';
        $this->load->view('backoffice/index', $data);
    }
}