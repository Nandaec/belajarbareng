<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function index()
    {
        $data['title']       = 'Login Admin';
        $data['description'] = 'Halaman Login Admin';
        $data['keywords']    = 'Login';
        $data['page']        = 'backoffice/login';
        $this->load->view('backoffice/temp_login', $data);

        if(isset($_POST['login']))
        {
            $username = $this->input->post('username');
            $password = md5($this->input->post('password'));

            $query = $this->db->query("SELECT * FROM p_administrator WHERE username='$username'")->row_array();

            if(empty($query))
            {
                $this->session->set_flashdata('callout-danger', 'User tidak ditemukan.');
                redirect('backoffice/login');
            }
            else
            {
                if($password == $query['password'])
                {
                    $session = array(
                        'id_admin' => $query['id_admin'],
                        'username' => $query['username'],
                        'logged_in'=> TRUE
                        );
                    $this->session->set_userdata($session);

                    $this->session->set_flashdata('toast-success', 'Berhasil Login.');
                    redirect('backoffice/dashboard', 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('callout-danger', 'Password yang anda masukan salah.');
                    redirect('backoffice/login');
                }
            }
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('id_admin');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('logged_in');
        $this->session->sess_destroy();

        $this->session->set_flashdata('callout-success', 'Berhasil Logout.');
        redirect('backoffice/login');
    }
}
