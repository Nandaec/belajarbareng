<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unit extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->security_model->loggedin_check_admin();
    }

	public function index()
	{
		$data['title']       = 'Data Unit';
        $data['description'] = 'Data Unit';
        $data['keywords']    = '';

        /* Pagination */
        $config['base_url']=base_url('admin/unit/index');
        $config['total_rows']=$this->main_model->Count_total('online_master_unit');
        $config['per_page']=10;  

        include 'Pagination_style.php';

        $from=$this->uri->segment(4);
        $this->pagination->initialize($config);
        $data['unit']=$this->main_model->Pagination($config['per_page'],$from,'online_master_unit');
        $data['num']=$from;

        // $data['unit'] = $this->db->query("SELECT * FROM online_master_unit")->result();
        $data['page']        = 'admin/unit';
        $this->load->view('admin/index', $data);
    }

    public function Edit_unit()
    {
        $id_record = $this->input->post('id_record');

        $datt=array(
            // 'unit'              => $this->input->post('unit'),
            'id_record_direksi' => $this->input->post('direksi')
            );
        $query = $this->main_model->update_something('online_master_unit',$datt,'id_record',$id_record);
        if ($query)
        {
            $this->session->set_flashdata('toast-success', 'Disposisi Berhasil Disimpan');
            redirect('admin/unit');
        }
        else
        {
            $this->session->set_flashdata('toast-error', 'Disposisi Gagal Disimpan');
            redirect('admin/unit');
        }
    }

    public function Hapus($id_record)
	{
		$data=array('id_record' => $id_record);
        $this->main_model->delete_something('online_master_unit',$data);
        $this->session->set_flashdata('toast-error', 'Berhasil Menghapus data');
        redirect("admin/unit");
	}

    public function Search()
    {
        $input = $this->input->post('search');
        $jum = $this->db->query("SELECT * FROM online_master_unit WHERE unit LIKE '%$input%'")->num_rows();

        $data['title']       = 'Pencarian Data Unit';
        $data['description'] = 'Jumlah Pencarian Data Unit <strong>'.$input.'</strong> berjumlah <strong>'.$jum.'</strong>';
        $data['keywords']    = '';
        $key=$this->input->post('search');
        $data['num']=0;
        $data['unit']=$this->main_model->search_unit($key);
        $data['page']        = 'admin/unit';
        $this->load->view('admin/index', $data);
    }
}