<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Direksi extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->security_model->loggedin_check_admin();
    }

	public function index()
	{
		$data['title']       = 'Data Direksi';
        $data['description'] = 'Data Direksi';
        $data['keywords']    = '';


        $data['direksi'] = $this->db->query("SELECT * FROM online_master_direksi")->result();
        $data['page']        = 'admin/direksi';
        $this->load->view('admin/index', $data);
    }

    public function Edit_direksi()
    {
        $id_record      = $this->input->post('id_record');
        $data['nama']   = $this->input->post('nama');
        $data['nama_lengkap']   = $this->input->post('nama_lengkap');

        $query = $this->main_model->update_something('online_master_direksi', $data, 'id_record', $id_record);
        if ($query)
        {
            $this->session->set_flashdata('toast-success', 'Berhasil menyimpan data');
            redirect('admin/direksi');
        }
        else
        {
            $this->session->set_flashdata('toast-error', 'Gagal menyimpan data');
            redirect('admin/direksi');
        }
    }

    public function Search()
    {
        $input = $this->input->post('search');
        $jum = $this->db->query("SELECT * FROM online_master_direksi WHERE nama LIKE '%$input%'")->num_rows();

        $data['title']       = 'Pencarian Data Direksi';
        $data['description'] = 'Jumlah Pencarian Data Direksi <strong>'.$input.'</strong> berjumlah <strong>'.$jum.'</strong>';
        $data['keywords']    = '';
        $key=$this->input->post('search');
        $data['num']=0;
        $data['direksi']=$this->main_model->search($key,'nama','online_master_direksi');
        $data['page']        = 'admin/direksi';
        $this->load->view('admin/index', $data);
    }

    public function Hapus($id_record)
	{
		$data=array('id_record' => $id_record);
        $this->main_model->delete_something('online_master_direksi',$data);
        $this->session->set_flashdata('toast-error', 'Berhasil Menghapus data');
        redirect("admin/direksi");
	}
}