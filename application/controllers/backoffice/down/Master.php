<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->security_model->loggedin_check_admin();
	}

    public function Pilih_unit()
	{
		$id_unit = $this->input->post('id_record_unit');
		$url = $this->input->post('url');

		if($url=='sarmut'){
			/*Pilih database*/
			$local = $this->load->database('localhost', TRUE);
			$data['data'] = $local->query("SELECT * FROM master_sasaran_mutu WHERE id_record_unit='$id_unit'")->result(); 
			/*Batas pilih database*/

			$data['id_unit']	= $id_unit;
			$data['page']       = 'admin/sasaran_mutu';
			$this->load->view('admin/index', $data);
		}elseif($url=='program_unit'){
			/*Pilih database*/
			$local = $this->load->database('localhost', TRUE);
			$data['data'] = $local->query("SELECT * FROM master_program_unit WHERE id_record_unit='$id_unit'")->result(); 
			/*Batas pilih database*/

			$data['id_unit']	= $id_unit;
			$data['page']       = 'admin/program_unit';
			$this->load->view('admin/index', $data);
		}elseif($url=='program_diluar_unit'){
			/*Pilih database*/
			$local = $this->load->database('localhost', TRUE);
			$data['data'] = $local->query("SELECT * FROM master_diluar_program_unit WHERE id_record_unit='$id_unit'")->result(); 
			/*Batas pilih database*/

			$data['id_unit']	= $id_unit;
			$data['page']       = 'admin/program_diluar_unit';
			$this->load->view('admin/index', $data);
		}else{
			$this->session->set_flashdata('toast-error', 'Tidak ada data unit');
			redirect('admin/master/sarmut');
		}
    }

	public function Sarmut()
	{
		$data['title']       = 'Data Master Sasaran Mutu';
        $data['description'] = 'Data Master Sasaran Mutu';
        $data['keywords']    = '';
        
        /*Pilih database ----> INI CUMA MANIPULASI*/
		$local = $this->load->database('localhost', TRUE);
		$data['data'] = $local->query("SELECT * FROM master_sasaran_mutu WHERE id_record_unit='0'")->result(); 
		/*Batas pilih database ----> INI CUMA MANIPULASI*/

        $data['page']        = 'admin/sasaran_mutu';
        $this->load->view('admin/index', $data);
    }

    public function Program_unit()
	{
		$data['title']       = 'Data Master Program Unit';
        $data['description'] = 'Data Master Program Unit';
        $data['keywords']    = '';
        $data['data']		 = '';

        /*Pilih database ----> INI CUMA MANIPULASI*/
		$local = $this->load->database('localhost', TRUE);
		$data['data'] = $local->query("SELECT * FROM master_program_unit WHERE id_record_unit='0'")->result(); 
		/*Batas pilih database ----> INI CUMA MANIPULASI*/

        $data['page']        = 'admin/program_unit';
        $this->load->view('admin/index', $data);
    }

    public function Program_diluar_unit()
	{
		$data['title']       = 'Data Master Sasaran Mutu';
        $data['description'] = 'Data Master Sasaran Mutu';
        $data['keywords']    = '';
        
        /*Pilih database ----> INI CUMA MANIPULASI*/
		$local = $this->load->database('localhost', TRUE);
		$data['data'] = $local->query("SELECT * FROM master_sasaran_mutu WHERE id_record_unit='0'")->result(); 
		/*Batas pilih database ----> INI CUMA MANIPULASI*/

        $data['page']        = 'admin/program_diluar_unit';
        $this->load->view('admin/index', $data);
    }



    public function Tambah_data_sarmut()
	{
		$id_unit = $this->input->post('id_record_unit');

		// $data = array(
		// 	'id_record_unit'=> $id_unit,
		// 	'sarmut'		=> $this->input->post('sasaran_mutu'),
		// 	'indikator'		=> $this->input->post('indikator'),
		// 	'target'		=> $this->input->post('target'),
		// 	'status'		=> '1'
		// 	);		
		// $query = $this->main_model->insert_something('master_sasaran_mutu', $data);

		/*Besok kalo db udah di 33 ini yang diganti insertnya dg yg diatas*/
		$sasaran_mutu 	= $this->input->post('sasaran_mutu');
		$indikator 		= $this->input->post('indikator');
		$target 		= $this->input->post('target');
		$local = $this->load->database('localhost', TRUE);
		$query = $local->query("
			INSERT INTO master_sasaran_mutu (id_record_unit, sarmut, indikator, target, status)
			VALUES ('$id_unit', '$sasaran_mutu', '$indikator', '$target', '1')");
		/*Batas Besok kalo db udah di 33 ini yang diganti insertnya dg yg diatas*/
		if ($query)
		{
			$this->session->set_flashdata('toast-success', 'Data Berhasil Tersimpan');

			/*Pilih database*/
			$local = $this->load->database('localhost', TRUE);
			$data['data'] = $local->query("SELECT * FROM master_sasaran_mutu WHERE id_record_unit='$id_unit'")->result(); 
			/*Batas pilih database*/

			$data['id_unit']	= $id_unit;
			$data['page']       = 'admin/sasaran_mutu';
			$this->load->view('admin/index', $data);
		}
		else
		{
			$this->session->set_flashdata('toast-error', 'Data Gagal Tersimpan');
			redirect('admin/master_sarmut');
		}
    }

    public function Tambah_data_program()
	{
		$id_unit = $this->input->post('id_record_unit');

		// $data = array(
		// 	'id_record_unit'=> $id_unit,
		// 	'sarmut'		=> $this->input->post('sasaran_mutu'),
		// 	'indikator'		=> $this->input->post('indikator'),
		// 	'target'		=> $this->input->post('target'),
		// 	'pencapaian'	=> $this->input->post('pencapaian'),
		// 	'keterangan'	=> $this->input->post('keterangan'),
		// 	'status_program'=> $this->input->post('status_program'),
		// 	'status'		=> '1'
		// 	);		

		$sasaran_mutu 	= $this->input->post('sasaran_mutu');
		$indikator 		= $this->input->post('indikator');
		$target 		= $this->input->post('target');
		$pencapaian 	= $this->input->post('pencapaian');
		$keterangan 	= $this->input->post('keterangan');
		$status_program = $this->input->post('status_program');

		$local = $this->load->database('localhost', TRUE);
		$query = $local->query("
			INSERT INTO master_program_unit (id_record_unit, sarmut, indikator, target, pencapaian, keterangan, status_program, status)
			VALUES ('$id_unit', '$sasaran_mutu', '$indikator', '$target', '$pencapaian','$keterangan','$status_program', '1')");;

		if ($query)
		{
			$this->session->set_flashdata('toast-success', 'Data Berhasil Tersimpan');

			/*Pilih database*/
			$local = $this->load->database('localhost', TRUE);
			$data['data'] = $local->query("SELECT * FROM master_program_unit WHERE id_record_unit='$id_unit'")->result(); 
			/*Batas pilih database*/

			$data['id_unit']	= $id_unit;
			$data['page']       = 'admin/program_unit';
			$this->load->view('admin/index', $data);
		}
		else
		{
			$this->session->set_flashdata('toast-error', 'Data Gagal Tersimpan');
			redirect('admin/master_program_unit');
		}
	}

	public function Tambah_program_diluar_unit()
	{
		$id_unit = $this->input->post('id_record_unit');

		$program 	= $this->input->post('program');
		$urgensi 	= $this->input->post('urgensi');
		$pencapaian = $this->input->post('pencapaian');

		$local = $this->load->database('localhost', TRUE);
		$query = $local->query("
			INSERT INTO master_diluar_program_unit (id_record_unit, program, urgensi, pencapaian)
			VALUES ('$id_unit', '$program', '$urgensi', '$pencapaian')");;

		if ($query)
		{
			$this->session->set_flashdata('toast-success', 'Data Berhasil Tersimpan');

			/*Pilih database*/
			$local = $this->load->database('localhost', TRUE);
			$data['data'] = $local->query("SELECT * FROM master_diluar_program_unit WHERE id_record_unit='$id_unit'")->result(); 
			/*Batas pilih database*/

			$data['id_unit']	= $id_unit;
			$data['page']       = 'admin/program_diluar_unit';
			$this->load->view('admin/index', $data);
		}
		else
		{
			$this->session->set_flashdata('toast-error', 'Data Gagal Tersimpan');
			redirect('admin/master_program_unit');
		}
	}

    public function Edit_data_sarmut()
	{
		$id = $this->input->post('id');
		$id_unit = $this->input->post('id_record_unit');
		// $datt=array(
		// 	'sasaran_mutu'	=> $this->input->post('sasaran_mutu'),
		// 	'indikator' 	=> $this->input->post('indikator'),
		// 	'target' 		=> $this->input->post('target')
		// 	);
		// $query = $this->main_model->update_something('master_sasaran_mutu',$datt,'id',$id);

		/*Besok kalo db udah di 33 ini yang diganti insertnya dg yg diatas*/
		$sasaran_mutu 	= $this->input->post('sasaran_mutu');
		$indikator 		= $this->input->post('indikator');
		$target 		= $this->input->post('target');
		$local = $this->load->database('localhost', TRUE);
		$query = $local->query("UPDATE master_sasaran_mutu SET 
			sarmut='$sasaran_mutu',
			indikator='$indikator',
			target='$target'
			where id='$id'");

        if ($query)
        {
            $this->session->set_flashdata('toast-success', 'Disposisi Berhasil Disimpan');

            /*Pilih database*/
			$local = $this->load->database('localhost', TRUE);
			$data['data'] = $local->query("SELECT * FROM master_sasaran_mutu WHERE id_record_unit='$id_unit'")->result(); 
			/*Batas pilih database*/

            $data['id_unit']	= $id_unit;
			$data['page']       = 'admin/sasaran_mutu';
			$this->load->view('admin/index', $data);
        }
        else
        {
            $this->session->set_flashdata('toast-error', 'Disposisi Gagal Disimpan');
            $data['id_unit']	= $id_unit;
			$data['page']       = 'admin/sasaran_mutu';
			$this->load->view('admin/index', $data);
        }
	}

	public function Edit_data_program()
	{
		$id = $this->input->post('id');
		$id_unit = $this->input->post('id_record_unit');
		// $datt=array(
		// 	'sarmut'		=> $this->input->post('sasaran_mutu'),
		// 	'indikator'		=> $this->input->post('indikator'),
		// 	'target'		=> $this->input->post('target'),
		// 	'pencapaian'	=> $this->input->post('pencapaian'),
		// 	'keterangan'	=> $this->input->post('keterangan'),
		// 	'status_program'=> $this->input->post('status_program'),
		// 	'status'		=> '1'
		// 	);
		// $query = $this->main_model->update_something('master_program_unit',$datt,'id',$id);

		/*Besok kalo db udah di 33 ini yang diganti insertnya dg yg diatas*/
		$sasaran_mutu 	= $this->input->post('sasaran_mutu');
		$indikator 		= $this->input->post('indikator');
		$target 		= $this->input->post('target');
		$pencapaian 	= $this->input->post('pencapaian');
		$keterangan 	= $this->input->post('keterangan');
		$status_program = $this->input->post('status_program');
		$local = $this->load->database('localhost', TRUE);
		$query = $local->query("UPDATE master_program_unit SET 
			sarmut='$sasaran_mutu',
			indikator='$indikator',
			target='$target',
			pencapaian='$pencapaian',
			keterangan='$keterangan',
			status_program='$status_program'
			where id='$id'");

        if ($query)
        {
            $this->session->set_flashdata('toast-success', 'Disposisi Berhasil Disimpan');

            /*Pilih database*/
			$local = $this->load->database('localhost', TRUE);
			$data['data'] = $local->query("SELECT * FROM master_program_unit WHERE id_record_unit='$id_unit'")->result(); 
			/*Batas pilih database*/

            $data['id_unit']	= $id_unit;
			$data['page']       = 'admin/program_unit';
			$this->load->view('admin/index', $data);
        }
        else
        {
            $this->session->set_flashdata('toast-error', 'Disposisi Gagal Disimpan');
            $data['id_unit']	= $id_unit;
			$data['page']       = 'admin/program_unit';
			$this->load->view('admin/index', $data);
        }
	}

	public function Edit_program_diluar_unit()
	{
		$id = $this->input->post('id');
		$id_unit = $this->input->post('id_record_unit');

		$program 	= $this->input->post('program');
		$urgensi 	= $this->input->post('urgensi');
		$pencapaian = $this->input->post('pencapaian');

		$local = $this->load->database('localhost', TRUE);
		$query = $local->query("UPDATE master_diluar_program_unit SET 
			program='$program',
			urgensi='$urgensi',
			pencapaian='$pencapaian'
			where id='$id'");

        if ($query)
        {
            $this->session->set_flashdata('toast-success', 'Disposisi Berhasil Disimpan');

            /*Pilih database*/
			$local = $this->load->database('localhost', TRUE);
			$data['data'] = $local->query("SELECT * FROM master_diluar_program_unit WHERE id_record_unit='$id_unit'")->result(); 
			/*Batas pilih database*/

            $data['id_unit']	= $id_unit;
			$data['page']       = 'admin/program_diluar_unit';
			$this->load->view('admin/index', $data);
        }
        else
        {
            $this->session->set_flashdata('toast-error', 'Disposisi Gagal Disimpan');
            $data['id_unit']	= $id_unit;
			$data['page']       = 'admin/program_diluar_unit';
			$this->load->view('admin/index', $data);
        }
	}

	public function Hapus($id,$id_unit)
	{
		// $hapus=array('id' => $id);
		// $this->main_model->delete_something('master_sasaran_mutu',$hapus);

        $local = $this->load->database('localhost', TRUE);
		$query = $local->query("DELETE FROM master_sasaran_mutu WHERE id = '$id'");
        
        $this->session->set_flashdata('toast-success', 'Berhasil Menghapus data');

        /*Pilih database*/
        $local = $this->load->database('localhost', TRUE);
        $data['data'] = $local->query("SELECT * FROM master_sasaran_mutu WHERE id_record_unit='$id_unit'")->result(); 
        /*Batas pilih database*/

        $data['id_unit']	= $id_unit;
        $data['page']       = 'admin/sasaran_mutu';
        $this->load->view('admin/index', $data);
	}

	public function Hapus_program($id,$id_unit)
	{
		// $hapus=array('id' => $id);
		// $this->main_model->delete_something('master_sasaran_mutu',$hapus);

        $local = $this->load->database('localhost', TRUE);
		$query = $local->query("DELETE FROM master_program_unit WHERE id = '$id'");
        
        $this->session->set_flashdata('toast-success', 'Berhasil Menghapus data');

        /*Pilih database*/
        $local = $this->load->database('localhost', TRUE);
        $data['data'] = $local->query("SELECT * FROM master_program_unit WHERE id_record_unit='$id_unit'")->result(); 
        /*Batas pilih database*/

        $data['id_unit']	= $id_unit;
        $data['page']       = 'admin/program_unit';
        $this->load->view('admin/index', $data);
	}

	public function Hapus_program_diluar_unit($id,$id_unit)
	{
        $local = $this->load->database('localhost', TRUE);
		$query = $local->query("DELETE FROM master_diluar_program_unit WHERE id = '$id'");
        
        $this->session->set_flashdata('toast-success', 'Berhasil Menghapus data');

        /*Pilih database*/
        $local = $this->load->database('localhost', TRUE);
        $data['data'] = $local->query("SELECT * FROM master_diluar_program_unit WHERE id_record_unit='$id_unit'")->result(); 
        /*Batas pilih database*/

        $data['id_unit']	= $id_unit;
        $data['page']       = 'admin/program_diluar_unit';
        $this->load->view('admin/index', $data);
	}
}