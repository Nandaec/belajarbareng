<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proses_disposisi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->security_model->loggedin_check_admin();
	}
	
	public function index()
	{		
		$data['title']       = 'Data Disposisi';
        $data['description'] = 'Halaman Data Disposisi';
        $data['keywords']    = 'Data Disposisi';

        /* Pagination */
        $config['base_url']=base_url('admin/proses_disposisi/index');
        $config['total_rows']=$this->main_model->Count_total('online_proses_kirim_disposisi');
        $config['per_page']=10;

        include 'Pagination_style.php';

        $from=$this->uri->segment(3);
        $this->pagination->initialize($config);
        $data['num']=$from;

        $limit = $config['per_page'];
        $data['proses_disposisi'] = $this->db->query("SELECT * FROM online_proses_kirim_disposisi LIMIT $limit")->result();

        $data['page']        = 'admin/proses_disposisi';
        $this->load->view('admin/index', $data);
	}

	public function Kirim()
	{
		$id_record_surat_disposisi = $this->input->post('id_record_surat_disposisi');
		$unit_dari = $this->input->post('unit_dari');
		$unit_kepada = $this->input->post('unit_kepada');

		$datt=array(
			'status_kirim_oleh_sekret'=> 't',
			'date_kirim_oleh_sekret'  => date_create('now', timezone_open('Asia/Jakarta'))->format('Y-m-d H:i:s')
			);
		$query = $this->main_model->update_something('online_proses_kirim_disposisi',$datt,'id_record_surat_disposisi',$id_record_surat_disposisi);

		if ($query)
		{
			$this->session->set_flashdata('toast-success','Berhasil mengirim surat kepada'.$unit_kepada);
			redirect('admin/proses_disposisi');
		}
		else
		{
			$this->session->set_flashdata('toast-error','Gagal mengirim surat kepada'.$unit_kepada);
			redirect('admin/proses_disposisi');
		}
	}
}