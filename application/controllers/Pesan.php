<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pesan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->security_model->loggedin_check();
	}
	
	public function index()
	{
        $id_karyawan = $this->session->userdata('id_record');

        /* Pagination */
        $config['base_url']=base_url('pesan/index');
        $config['total_rows']=$this->main_model->Count_total('online_proses_kirim_disposisi');
        $config['per_page']=10;
        /*Tampilan Pagination*/
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination pagination-sm justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link" style="background-color: #007bff; color: #ffffff;">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
        /*===============*/
        $from=$this->uri->segment(3);
        $this->pagination->initialize($config);
        // $data['pesan']=$this->main_model->Pagination_laporan($config['per_page'],$from,'d_kirim_disposisi');
        $data['num']=$from;

        $limit = $config['per_page'];
        $data['pesan'] = $this->db->query("SELECT * FROM online_proses_kirim_disposisi where id_record_karyawan_to = '$id_karyawan' order by date_kirim_oleh_sekret DESC LIMIT $limit")->result();

		$data['title']       = 'Kotak Masuk';
        $data['description'] = 'Halaman Kotak Masuk';
        $data['keywords']    = '';
        $data['page']        = 'pesan';
        $this->load->view('index', $data);
	}

    public function Balas($id_record=0)
    {
        // $data['status_pesan'] = 'dibaca';
        // $query = $this->main_model->update_something('d_kirim_disposisi', $data, 'id_record', $id_record);
        redirect('pesan');
    }

    public function pdf($id_record=0)
    {
        $this->load->library("pdf");
        
        $data['pesan'] = $this->db->query("SELECT * FROM online_surat_disposisi where id_record = '$id_record'")->result();
        $this->load->view("lihat_pdf",$data);
    }

    public function Search()
    {
        $input = $this->input->post('search');
        $jum = $this->db->query("SELECT * FROM `d_kirim_disposisi` WHERE judul LIKE '%$input%'")->num_rows();

        $data['title']       = 'Pencarian Kotak Masuk';
        $data['description'] = 'Data Pencarian Kotak Masuk <strong>'.$jum.'</strong>';
        $data['keywords']    = '';
        $key=$this->input->post('search');
        $data['num']=0;
        $data['pesan']=$this->main_model->search_pesan($key);
        $data['page']        = 'pesan';
        $this->load->view('index', $data);
    }
}
